$('#special-offer').live('click', function(){
    if($(this).is(':checked')){
        $('#quantity').val($(this).val());
    }else{
        $('#quantity').val(0);
    }
});

$('.show-hide-fields').live('click', function(){
    $('#'+$(this).attr('id')+'-fields').slideToggle();
});

$('.submit').live('click', function(){
    $(this).parents('form').submit();
});

$('input.quantity-field').live('change', function(){
    if($(this).val() == ''){
        $(this).val() = 1;
    }
    var defaultValue = $(this).attr('id').split('-');
    if($(this).val() > 3){
        alert('Please enter Quantity from 1 till 3');
        $(this).val(defaultValue[1]);
    }
});

$('form.discount-form').live('submit', function(){
    if($('input.required').val()==''){
        alert('Discount code can not be empty.');
        return false;
    }
});

$('select.country').live('change', function(){
    var currentElement = $(this);
    $.get("/cart/state/"+$(this).val()+'/'+$(this).attr('name'),{
    },function(data){
        currentElement.parents('fieldset').find('.state-container').html(data);
    });
});

$('input.copy-billing').live('click', function(){
    if($(this).is(':checked')){
        $('.shipping-container').find('input.first-name').val($('.billing-container').find('input.first-name').val());
        $('.shipping-container').find('input.last-name').val($('.billing-container').find('input.last-name').val());
        $('.shipping-container').find('select.country').val($('.billing-container').find('select.country').val());
        $('.shipping-container').find('input.address').val($('.billing-container').find('input.address').val());
        $('.shipping-container').find('input.city').val($('.billing-container').find('input.city').val());
        $('.shipping-container').find('input.zip').val($('.billing-container').find('input.zip').val());
        $('.shipping-container').find('input.phone').val($('.billing-container').find('input.phone').val());
        $('.shipping-container').find('input.email').val($('.billing-container').find('input.email').val());
                
        if($('.billing-container').find('.state-container select').hasClass('hidden')){
            $('.shipping-container').find('.state-container select').hide();
            $('.shipping-container').find('.state-container input:text').val($('.billing-container').find('.state-container input:text').val());
            $('.shipping-container').find('.state-container input:text').show();
        }else{
            $('.shipping-container').find('.state-container input:text').hide();
            $('.shipping-container').find('.state-container select').val($('.billing-container').find('.state-container select').val());
            $('.shipping-container').find('.state-container select').show();            
        }
        $('.shipping-container input:text').attr('readonly', true)
        $('.shipping-container select').attr('readonly', true)
        
    }else{
        $('.shipping-container input:text').attr('disabled', false);
        $('.shipping-container select').attr('disabled', false);
    }
    
});


$('.select-date-option').live('click', function(){
    if($(this).val() == 'yes'){
        $(".datepicker").removeAttr("disabled"); 
    }else{
        $(".datepicker").attr("disabled", "disabled"); 
    }
})

$('.cancel').live('click', function(){
    document.location = $(this).attr('href');
});

$('select.option-category').live('change', function(){
    var currentElement = $(this);
    $.post("/admin/store/edit-old-product-option/"+$(this).parents('tr').attr('id'),{'CategoryID':$(this).val(), 'GroupOptionID':0, 'OptionValueID':0
    },function(data){
        currentElement.parents('tr').html(data);
    });
});

$('select.option-group').live('change', function(){
    var currentElement = $(this);
    $.post("/admin/store/edit-old-product-option/"+$(this).parents('tr').attr('id'),{'GroupOptionID':$(this).val(), 'OptionValueID':0
    },function(data){
        currentElement.parents('tr').html(data);
    });
});


$('select.option-in-group').live('change', function(){
    var currentElement = $(this);
    $.post("/admin/store/edit-old-product-option/"+$(this).parents('tr').attr('id'),{'OptionValueID':$(this).val()
    },function(data){
        currentElement.parents('tr').html(data);
    });
});

$('select.vendor-list').live('change', function(){
    var cartId = $(this).attr('id').split('-')[1];
    
    $.post("/admin/order/update-cart/"+cartId,{'vendor':$(this).val()},function(data){
        alert('Vendor has chenged');
    });
});

$('a.message-to-vendor').live('click', function(){
    var cartId = $(this).parents('tr').attr('id');
    
    $.get("/admin/order/vendor-message/"+cartId,function(response){
        $("#email-box").html(response);
        $( "#email-box" ).dialog({height: 400});
        $("#email-box").dialog( "open" );        
    });
    return false;
});

$('a.email-to').live('click', function(){
    var url = $(this).attr('href');
    var modalheight = 400;
    var modalwidth = 600;
    var dimention = $(this).attr('name');
    if(dimention){
        dimention = dimention.split('-');
        modalwidth = dimention[0];
        modalheight = dimention[1];
    }
    $.get(url,function(response){
        $("#email-box").html(response);
        $( "#email-box" ).dialog({height: modalheight});
        $("#email-box").dialog( "open" );        
    });
    return false;
});

$('.email-form').live('submit', function(){
    var url = $(this).attr('action');
    var postData = $(this).serialize();
    $.post(url,postData,function(data){
        $("#email-box").dialog( "close" );
    });
    
    return false;
});
$('.dialog-close').live('click', function(){
    $("#email-box").dialog( "close" );  
});

$('.delete-item').live('click', function(){
    if(confirm("Are you sure to proceed")){
    }
    return false;
});

$('.cart-flag').live('click', function(){
    var cartId = $(this).parents('tr').attr('id');
    var fieldName = $(this).attr('name').split('-')[0];
    var supliesSent = "N";
    if($(this).is(':checked')){
        supliesSent = "Y";
    }
    var params = {'SuppliesSent':supliesSent};
    if('SampleReceived'==fieldName){
        params = {'SampleReceived':supliesSent};
    }else if('StockSent'==fieldName){
        params = {'StockSent':supliesSent};
    }
    $.post("/admin/order/update-cart/"+cartId,params,function(data){
        alert(fieldName + ' has chenged');
    });
});

$('.delete-order').live('click', function(){
    var cartId = $(this).parents('tr').attr('id');
    $.post("/admin/order/multiple-delete",{'ids':[cartId]},function(data){
        $('#'+cartId).remove();
    });
});

$('.multiple-delete').live('click', function(){
    var cartId = $(this).parents('tr').attr('id');
    var allVals = [];
    $('.table-row:checked').each(function() {
        allVals.push($(this).val());
    });
    if(allVals.length == 0){
        alert("No items are selected");
        return false;
    }
    $.post("/admin/order/multiple-delete",{'ids':allVals},function(data){
        $('.table-row:checked').each(function() {
            $('#'+$(this).val()).remove();
        });
    });
});