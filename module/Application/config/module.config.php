<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'static' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/static[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Static',
                        'action' => 'supersilk',
                    ),
                ),
            ),
            'faq' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/faq[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Faq',
                        'action' => 'index',
                    ),
                ),
            ),
            'custom-hair' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/custom-hair',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Product',
                        'action' => 'customhair',
                    ),
                ),
            ),
            'product-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product-category[/:id]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Product',
                        'action' => 'category',
                    ),
                ),
            ),
            'product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product[/:id]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Product',
                        'action' => 'index',
                    ),
                ),
            ),         
            'order' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/order[/:action][/:id]',
                    'constraints' => array(
                        'id'     => '[0-9-]+',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Order',
                        'action' => 'index',
                    ),
                ),
            ),
            'cart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cart[/:action][/:id][/:type]',
                    'constraints' => array(
                        'id'     => '[0-9-]+',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'type' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Cart',
                        'action' => 'index',
                    ),
                ),
            ),
            're-order' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/re-order[/:action][/:id]',
                    'constraints' => array(
                        'id'     => '[0-9-]+',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Reorder',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Static' => 'Application\Controller\StaticController',
            'Application\Controller\Faq' => 'Application\Controller\FaqController',
            'Application\Controller\Product' => 'Application\Controller\ProductController',
            'Application\Controller\Order' => 'Application\Controller\OrderController',
            'Application\Controller\Reorder' => 'Application\Controller\ReorderController',
            'Application\Controller\Cart' => 'Application\Controller\CartController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
