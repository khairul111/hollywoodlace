<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Application\Model\ProductTable,
    Application\Model\CategoryTable,
    Application\Model\OptionGroupTable,
    Application\Model\CartTable,
    Application\Model\ProductCartTable,
    Application\Model\CartProductOptionTable,
    Application\Model\DiscountCodeTable,
    Application\Model\CountryTable,
    Application\Model\StateTable,
    Application\Model\OptionTable;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $e->getApplication()->getServiceManager()->get('translator');
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        if (!ini_get('display_errors')) {
            ini_set('display_errors', '1');
        }
        define('StockUnitOrderEmailFrom','hollywoodlaceorders@yahoo.com');
        define('StockUnitOrderEmailTo','sales@suncolorhair.com');
        define('StockUnitOrderEmailCC','hollywoodlaceorders@yahoo.com');
        define('StockUnitOrderEmailBcc','');
        define('NotificationFrom','hollywoodlaceorders@yahoo.com');
        define('NotificationTo','hollywoodlaceorders@yahoo.com');
        define('PayPalAccount','hollywoodlaceorders@yahoo.com');

        date_default_timezone_set('America/Los_Angeles');
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Application\Model\ProductTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new ProductTable($dbAdapter);
                    return $table;
                },
                'Application\Model\CategoryTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new CategoryTable($dbAdapter);
                    return $table;
                },
                'Application\Model\OptionGroupTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new OptionGroupTable($dbAdapter);
                    return $table;
                },
                'Application\Model\OptionTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new OptionTable($dbAdapter);
                    return $table;
                },
                'Application\Model\CartTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new CartTable($dbAdapter);
                    return $table;
                },
                'Application\Model\ProductCartTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new ProductCartTable($dbAdapter);
                    return $table;
                },
                'Application\Model\CartProductOptionTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new CartProductOptionTable($dbAdapter);
                    return $table;
                },
                'Application\Model\DiscountCodeTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new DiscountCodeTable($dbAdapter);
                    return $table;
                },
                'Application\Model\CountryTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new CountryTable($dbAdapter);
                    return $table;
                },
                'Application\Model\StateTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new StateTable($dbAdapter);
                    return $table;
                },
            ),
                        
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    
}
