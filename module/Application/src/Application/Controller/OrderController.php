<?php

namespace Application\Controller;

use Application\Controller\BaseController;
use Application\Form\OrderForm;
use Zend\Session\Container;

class OrderController extends BaseController
{

    public function indexAction()
    {
        $productId = (int) $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        if($productId == 0){
            $products = $productTable->getByCategoryId(7);
            $productId = $products[0]['ProductID'];
        }
        $product = $productTable->getById($productId);
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $optionGroups = $optionGroupTable->getByProductId($productId);
        
        $formDependency = array('optionGroups' => $optionGroups, 'productId' =>$productId, 'categoryId'=>$product['CategoryID']);
        $orderForm = new OrderForm($sm, $formDependency);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $orderForm->setData($request->getPost());
            if ($orderForm->isValid()) {
                $cartId = $this->_process($orderForm->getData());
                return $this->redirect()->toRoute('order', array('action'=>'show'));
            }
        }
        
        return array('form'=>$orderForm, 'optionGroups'=>$optionGroups, 'product'=>$product);
    }
    
    public function showAction()
    {
        $session = new Container('hollywoodlace');
        $cartId = $this->params()->fromRoute('id', $session->cartId);
        $sm = $this->getServiceLocator();
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $cartTable = $sm->get('Application\Model\CartTable');
        $cart = $cartTable->getById($cartId);
        $newBilling = true;
        if($cart['PreviousOrderID'] != ''){
            $newBilling = false;
        }
        $productCarts = $productCartTable->getByCartId($cartId);
        foreach($productCarts as $key=>$productCart){
            $productCarts[$key]['options'] = $productCartOptionTable->getByProductCartId($productCart['CartProductID']);
        }
        return array('productCarts'=>$productCarts, 'cart'=>$cart, 'flashMessages'=>$this->flashMessenger()->getMessages(),'newBilling'=>$newBilling);
    }
    
    public function updatequantityAction()
    {
        $request = $this->getRequest();
        $data = $request->getPost();
        $sm = $this->getServiceLocator();
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $cartTable = $sm->get('Application\Model\CartTable');
        foreach($data['productQuantity'] as $key=>$quantity){
            $productCartTable->save(array('ProductQty'=>$quantity), $key);
        }
        $cartTable->save(array('DiscountCode'=>'','DiscountType'=>'','DiscountValue'=>'0.00'),$data['cartId']);
        $cartTable->recalculateCart($data['cartId'], $sm);
        return $this->redirect()->toRoute('order', array('action'=>'show'));
    }

    public function deleteAction()
    {
        $productCartId = $this->params()->fromRoute('id', 0);
        $productCartIds = explode('-', $productCartId);
        $sm = $this->getServiceLocator();
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $cartTable = $sm->get('Application\Model\CartTable');
        $productCartTable->deleteItem($productCartIds[0]);
        $cartTable->save(array('DiscountCode'=>'','DiscountType'=>'','DiscountValue'=>'0.00'),$productCartIds[1]);
        $cartTable->recalculateCart($productCartIds[1], $sm);
        return $this->redirect()->toRoute('order', array('action'=>'show'));
    }
    
    public function discountAction()
    {
        $request = $this->getRequest();
        $data = $request->getPost();
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $discountCodeTable = $sm->get('Application\Model\DiscountCodeTable');
        
        $cart = $cartTable->getById($data['cartId']);
        $code = str_replace("'", "", $data['discountCode']);
        $discountCode = $discountCodeTable->isValidCode($code);
        if(!$discountCode){
            $cartTable->save(array('DiscountCode'=>'','DiscountType'=>'','DiscountValue'=>'0.00'),$data['cartId']);
            $this->flashMessenger()->addMessage('Invalid Discount Code.');
            $cartTable->recalculateCart($data['cartId'], $sm);
            return $this->redirect()->toRoute('order', array('action'=>'show'));
        }
        
        if ($discountCode['OrderMinimum'] > $cart['PriceSubTotal']){
            $this->flashMessenger()->addMessage('Order Minimum is '.$discountCode['OrderMinimum'].' for your discount code');
            return $this->redirect()->toRoute('order', array('action'=>'show'));
        }
        
        $cartTable->save(array('DiscountCode'=>$code,'DiscountType'=>$discountCode['DiscountType'],'DiscountValue'=>$discountCode['DiscountValue']),$data['cartId']);
        $cartTable->recalculateCart($data['cartId'], $sm);
        return $this->redirect()->toRoute('order', array('action'=>'show'));
    }

    public function addAction()
    {
        $param = explode('-', $this->params()->fromRoute('id', 0));
        $productId = $param[0];
        $oldCartId = (array_key_exists('1', $param))?$param[1]:null;
        
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $productTable = $sm->get('Application\Model\ProductTable');
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $cartId = $this->_manageCart($cartTable);               
        $product = $productTable->getById($productId);
        $productCartData = array('CartID'=>$cartId, 'ProductID'=>$product['ProductID'],'CategoryID'=>$product['CategoryID'],
            'CartProductName'=>$product['ProductName'], 'ProductPrice1'=>$product['ProductPrice1'], 'ProductPrice2'=>$product['ProductPrice2'],
            'ProductPrice3'=>$product['ProductPrice3'], 'HairCutPrice'=>$product['HairCutPrice'], 'FullCapPrice'=>$product['FullCapPrice'],
            'ChengFullCapPrice'=>$product['ChengFullCapPrice'], 'LyndFullCapPrice'=>$product['LyndFullCapPrice'], 'ProductPriceCheng'=>$product['ProductPriceCheng'],
            'ProductPriceLynd'=>$product['ProductPriceLynd'], 'ProductShippingPrice1'=>$product['ShippingPrice1'], 'ProductShippingPrice2'=>$product['ShippingPrice2'],
            'ProductQty'=>1, 'SubTotalPrice'=>0, 'SubTotalShipping'=>0, 'TotalPrice'=>0,'LastUpdate'=>date("Y-m-d H:i:s"), 'IPAddress'=>$_SERVER['REMOTE_ADDR']);
        
        $productCart = $productCartTable->getByProductIdAndCartId($product['ProductID'], $cartId);
        if($productCart){
            $productCartTable->save($productCartData, $productCart['CartProductID']);
        }else{
            $productCartId = $productCartTable->save($productCartData);
        }
        if($oldCartId){
            $cartTable->save(array('Reorder'=>'Y','PreviousOrderID'=>$oldCartId), $cartId);
        }
        $cartTable->recalculateCart($cartId, $sm);
        return $this->redirect()->toRoute('order', array('action'=>'show'));
    }

    public function statusAction()
    {
        $request = $this->getRequest();
        $viewModel = new \Zend\View\Model\ViewModel();
        if (!$request->isPost()) {           
            $viewModel->setTemplate('application/partial/login');
            return $viewModel;
        }
        $email = $request->getPost('email');
        $sm = $this->getServiceLocator();
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $cartTable = $sm->get('Application\Model\CartTable');
        
        $carts = $cartTable->getActiveByBillingEmail($email);
        if(!$carts){
            $viewModel->setTemplate('application/partial/login');
            return $viewModel;
        }
        
        foreach($carts as $key=>$cart){
            if($cart['Imported'] == 'Y'){
                $carts[$key]['products'] = $cartTable->prepareImportedOrderText($cart['OrderTXT']);
                continue;
            }
            $carts[$key]['products'] = $productCartTable->getByCartId($cart['CartID']);
            foreach($carts[$key]['products'] as $productKey =>$product){
                $carts[$key]['products'][$productKey]['options'] = $productCartOptionTable->getByProductCartId($product['CartProductID']);
            }
        }
        $viewModel->setVariables(array('carts'=>$carts));
        return $viewModel;
    }
    
    
    private function _process($data)
    {
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $cartId = $this->_manageCart($cartTable);       
        $productCartId = $this->_manageProductCart($data, $cartId);
        $cartTable->recalculateCart($cartId, $sm);
        return $cartId;
    }
    
    private function _manageCart($cartTable, $oldCartId = 0)
    {
        $session = new Container('hollywoodlace');  
       
        if (isset($session->cartId) && intval($session->cartId) > 0) {
            $cartId = $session->cartId;
        }else{
            $cartId = $cartTable->save(array('SessionIPAddress'=>$_SERVER['REMOTE_ADDR'],'Added'=>date("Y-m-d H:i:s")));
            $session->cartId = $cartId;
        }
            
        return $cartId;
    }
    
    private function _manageProductCart($data, $cartId)
    {
        
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $optionTable = $sm->get('Application\Model\OptionTable');
        
        $product = $productTable->getById($data['hairType']);
        $specialOffer = 'N';
        if(in_array('specialOffer', $data) && $data['specialOffer'] = 1){
            $specialOffer = 'Y';
        }
        if (isset($_REQUEST['Quantity']) && intval($_REQUEST['Quantity']) > 0) {
            $ProductQty = intval($_REQUEST['Quantity']);
        } else {
            $ProductQty = 1;
        }
          
        $productCartData = array('CartID'=>$cartId, 'ProductID'=>$product['ProductID'],'CategoryID'=>$product['CategoryID'],
            'CartProductName'=>$product['ProductName'], 'ProductPrice1'=>$product['ProductPrice1'], 'ProductPrice2'=>$product['ProductPrice2'],
            'ProductPrice3'=>$product['ProductPrice3'], 'HairCutPrice'=>$product['HairCutPrice'], 'FullCapPrice'=>$product['FullCapPrice'],
            'ChengFullCapPrice'=>$product['ChengFullCapPrice'], 'LyndFullCapPrice'=>$product['LyndFullCapPrice'], 'ProductPriceCheng'=>$product['ProductPriceCheng'],
            'ProductPriceLynd'=>$product['ProductPriceLynd'], 'ProductShippingPrice1'=>$product['ShippingPrice1'], 'ProductShippingPrice2'=>$product['ShippingPrice2'],
            'ProductQty'=>(in_array('quantity', $data))?$data['quantity']:1, 'SubTotalPrice'=>0, 'SubTotalShipping'=>0, 'TotalPrice'=>0,'LastUpdate'=>date("Y-m-d H:i:s"), 'IPAddress'=>$_SERVER['REMOTE_ADDR'],
            'ExtraPrice1'=>($data['optional']==1)?$product['HairCutPrice']:0, 'ExtraPrice2'=>($data['fullcap']==1)?$product['FullCapPrice']:0, 'SpecilOffer'=>$specialOffer,
            'ProfessionalStyle'=>($data['optional']==1)? 'Y':'N', 'FrontLength'=>$data['frontLength'], 'SideLength'=>$data['sideLength'],
            'BackLength'=>$data['backLength'], 'TopLength'=>$data['topLength'], 'AdditionalInstructions'=>$data['instruction']);
        
        
        if(array_key_exists('fullcap', $data)){
            $productCartData['FullCap'] = ($data['fullcap']== 1)?'Y':'N';
            for($i=1; $i<=7; $i++){
                $productCartData['Measurement'.$i] = $data['measurement'.$i];
            }
        }
        
        $productCart = $productCartTable->getByProductIdAndCartId($product['ProductID'], $cartId);
        if($productCart){
            $productCartTable->save($productCartData, $productCart['CartProductID']);
            $productCartId = $productCart['CartProductID'];
        }else{
            $productCartId = $productCartTable->save($productCartData);
            $optionGroups = $optionGroupTable->getByProductId($data['hairType']);
            foreach($optionGroups as $optionGroup){
                $option = $optionTable->getById($data['GroupOptionID'.$optionGroup['OptionGroupID']]);
                if(!$option) continue;
                $cartProductOption = array('CartProductID'=>$productCartId,'OptionGroupID'=>$optionGroup['OptionGroupID'],'OptionGroupName'=>$optionGroup['OptionGroupName'],
                    'OptionGroupSortKey'=>$optionGroup['SortKey'],'OptionID'=>$option['OptionID'],'OptionName'=>$option['OptionName'],'AddPrice'=>$option['AddPrice'],
                    'PriceCheng'=>$option['PriceCheng'],'PriceLynd'=>$option['PriceLynd'],'LastUpdate'=>date("Y-m-d H:i:s"),'IPAddress'=>$_SERVER['REMOTE_ADDR']);

                $productCartUpdateData = array();
                if (strpos(strtolower('_' . $optionGroup['OptionGroupName']), "shipping") > 0) {
                    $productCartUpdateData = array('ProductShippingPrice1'=>$option['AddPrice'], 'ProductShippingPrice2'=>$option['AddPrice']);
                    $cartProductOption['AddPrice'] = 0;
                }

                if (strpos(strtolower('_' . $optionGroup['OptionGroupName']), "quantity") > 0) {
                    $productQuantity = intval(trim(substr($option['OptionName'], 0, 2)));
                    if($productQuantity > 1){
                        $productCartUpdateData['ProductQty'] = $productQuantity;
                    }
                }
                $productCartTable->save($productCartUpdateData, $productCartId);
                $productCartOptionId = $productCartOptionTable->save($cartProductOption);
            }
        }
        
        return $productCartId;
    }
    
}
