<?php

namespace Application\Controller;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class BaseController extends AbstractActionController
{
    public $_data = array();
    public $_view;
    
    public function setEventManager(EventManagerInterface $events) {
        parent::setEventManager($events);

        $controller = $this;
        $events->attach('dispatch', function ($e) use ($controller) {
                    call_user_func(array($controller,'loadLayout'));
                }, 100); 
    }
    
    public function loadLayout()
    { 
        $this->_prepareMainMenu();
        $layout = $this->layout();
        $mainMenuView = new ViewModel($this->_data);
        $mainMenuView->setTemplate('layout/mainmenu');
        $layout->addChild($mainMenuView, 'mainMenu');
    }
    
    private function _prepareMainMenu()
    {
        $menu = array();
        
        $menu['']['title'] = 'Home';
        $menu['static/accessories']['title'] = 'Accessories';
        
        $menu['custom-hair']['title'] = 'Hair System';
        $menu['custom-hair']['child'] = array('static/haircolor'=>'Hair Color', 'static/hairdensity'=>'Density',
            'static/hairstyle'=>'Style', 'static/hairtypes'=>'Hair Types',
            'static/deliverypriority'=>'Delivery Priority', 'static/wavecurl'=>'Wave/Curl', 'static/basesizing'=>'Base Sizing');
        
        $menu['faq']['title'] = 'FAQ';
        $menu['faq']['child'] = array('faq/general'=>'General Question', 'faq/attachment'=>'Attachment',
            'faq/fitting'=>'Fitting and Sizing', 'static/accessories'=>'Accessories',
            'faq/systemcare'=>'System Care', 'custom-hair'=>'Hair System', 
            'static/haircolor'=>'Hair Color', 'static/hairdensity'=>'Density',
            'static/hairstyle'=>'Style', 'static/hairtypes'=>'Hair Types', 
            'static/deliverypriority'=>'Delivery Priority', 'static/wavecurl'=>'Wave/Curl', 'faq/fitting'=>'Base Sizing');
        
        $menu['static/aboutus']['title'] = 'About Us';
        $menu['static/contact']['title'] = 'Contact Us';
        
        $menu['order']['title'] = 'Order';
        $menu['order']['child'] = array('order'=>'Order Now', 're-order'=>'Re-Order',
            'order/status'=>'Check Order Status', '#order-help'=>'Order Help');
        
        $this->_data['mainMenu'] = $menu;
    }
        
}
