<?php

namespace Application\Controller;

use Application\Controller\BaseController;
use Application\Form\BillingForm;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class CartController extends BaseController 
{

    private $_cartId;
    public function billingAction() 
    {
        $this->_checkCartId();
        $sm = $this->getServiceLocator();
        $paymentType = $this->params()->fromRoute('type', 'creditCard');
        $billingForm = new BillingForm($sm, array('paymentMethod'=>$paymentType));       
        $request = $this->getRequest();
        if ($request->isPost()) {
            $billingForm->setData($request->getPost());
            if ($billingForm->isValid()) {
                $this->_process($billingForm->getData());
                return $this->redirect()->toRoute('cart', array('action'=>'checkout'));
            }
        }
        
        return array('form'=>$billingForm);
    }
    
    public function editBillingAction(){
        $this->_checkCartId();
        $sm = $this->getServiceLocator();
        $paymentType = $this->params()->fromRoute('type', 'creditCard');
        $cartTable = $sm->get('Application\Model\CartTable');
        $cart = $cartTable->getById($this->_cartId);
        if($cart['PreviousOrderID'] != '' && $cart['BillingFirstName'] ==''){
            $cart = $cartTable->getById($cart['PreviousOrderID']);
        }
        
        $billingForm = new BillingForm($sm, array('paymentMethod'=>$paymentType, 'cart'=>$cart));
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $billingForm->setData($request->getPost());
            $this->_process($request->getPost());
            return $this->redirect()->toRoute('cart', array('action'=>'checkout'));

        }
        $result = new ViewModel(array('form'=>$billingForm));
        $result->setTemplate('application/cart/billing');
        return $result;
    }
    
    public function stateAction()
    {
        $result = new ViewModel();
        $result->setTerminal(true);
        $countryId = (int) $this->params()->fromRoute('id');
        $countryType = $this->params()->fromRoute('type', 'billingCountry');
        $sm = $this->getServiceLocator();
        $stateTable = $sm->get('Application\Model\StateTable');
        $states = $stateTable->getByICountryd($countryId);
        
        $stateFieldName = ($countryType == 'shippingCountry')?'shippingState':'billingState';
        $stateFieldTextName = ($countryType == 'shippingCountry')?'shippingStateName':'billingStateName';
        
        $result->setVariables(array('states' => $states, 'fieldName'=>$stateFieldName, 'fieldTextName'=>$stateFieldTextName));
        return $result;
        
    }

    public function checkoutAction()
    {
        $session = new Container('hollywoodlace');
        $cartId = $session->cartId;
        $sm = $this->getServiceLocator();
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $cartTable = $sm->get('Application\Model\CartTable');
        $countryTable = $sm->get('Application\Model\CountryTable');
        $stateTable = $sm->get('Application\Model\StateTable');
        
        $cart = $cartTable->getById($cartId);
        $cart['billingCountry'] = $countryTable->getNameById($cart['BillingCountryID']);
        $cart['shippingCountry'] = $countryTable->getNameById($cart['ShippingCountryID']);
        $cart['billingState'] = $cart['BillingStateName'];
        $cart['shippingState'] = $cart['ShippingStateName'];
        if($cart['BillingStateID']){
            $cart['billingState'] = $stateTable->getNameById($cart['BillingStateID']);
        }
        
        if($cart['BillingStateID']){
            $cart['shippingState'] = $stateTable->getNameById($cart['ShippingStateID']);
        }
        $productCarts = $productCartTable->getByCartId($cartId);
        foreach($productCarts as $key=>$productCart){
            $productCarts[$key]['options'] = $productCartOptionTable->getByProductCartId($productCart['CartProductID']);
        }
        
        $responseMessage = null;
        $creditCardForm = new \Application\Form\CreditCardPaymentForm();        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $validator = new \Application\Model\Validator();
            $creditCardForm->setInputFilter($validator->getInputFilter());
            $creditCardForm->setData($request->getPost());
            if ($creditCardForm->isValid()) {
                $checkoutModel = new \Application\Model\Checkout($sm, $this->getServiceLocator()->get('ViewRenderer'));
                if($checkoutModel->payWithCreditCard($creditCardForm->getData(), $cartId, $responseMessage)){
                    $this->_sentAdminMail($cart, $productCarts);
                    $this->_sentCustomerMail($cart, $productCart);
                    return $this->redirect()->toRoute('cart', array('action'=>'completepayment','cartId'=>$cartId));
                }
                
            }
        } 
        return array('productCarts'=>$productCarts, 'cart'=>$cart, 'form'=>$creditCardForm, 'responseMessage'=>$responseMessage);
    }
    
    public function paypalAction(){
        $session = new Container('hollywoodlace');
        $cartId = $session->cartId;
        $sm = $this->getServiceLocator();
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $cartTable = $sm->get('Application\Model\CartTable');
        $countryTable = $sm->get('Application\Model\CountryTable');
        $stateTable = $sm->get('Application\Model\StateTable');
        
        $cart = $cartTable->getById($cartId);
        $cart['billingCountry'] = $countryTable->getNameById($cart['BillingCountryID']);
        $cart['shippingCountry'] = $countryTable->getNameById($cart['ShippingCountryID']);
        $cart['billingState'] = $cart['BillingStateName'];
        $cart['shippingState'] = $cart['ShippingStateName'];
        $productCarts = $productCartTable->getByCartId($cartId);
        foreach($productCarts as $key=>$productCart){
            $options = $productCartOptionTable->getByProductCartId($productCart['CartProductID']);
            $optionsInfo = '';
            foreach($options as $option){
                $optionsInfo .= $option['OptionGroupName'].':'.$option['OptionName'].';';
            }
            $productCarts[$key]['option'] = $optionsInfo;
        }
        
        return array('productCarts'=>$productCarts, 'cart'=>$cart);
        
    }


    public function completepaymentAction(){
        
        var_dump($this->getRequest()->getPost());die;
        $session = new Container('hollywoodlace');
        $cartId = $this->params()->fromRoute('cartId', $session->cartId);
        $sm = $this->getServiceLocator();
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $cartTable = $sm->get('Application\Model\CartTable');
        $countryTable = $sm->get('Application\Model\CountryTable');
        $stateTable = $sm->get('Application\Model\StateTable');
        
        $cart = $cartTable->getById($cartId);
        $cart['billingCountry'] = $countryTable->getNameById($cart['BillingCountryID']);
        $cart['shippingCountry'] = $countryTable->getNameById($cart['ShippingCountryID']);
        $cart['billingState'] = $cart['BillingStateName'];
        $cart['shippingState'] = $cart['ShippingStateName'];
        if($cart['BillingStateID']){
            $cart['billingState'] = $stateTable->getNameById($cart['BillingStateID']);
        }
        
        if($cart['BillingStateID']){
            $cart['shippingState'] = $stateTable->getNameById($cart['ShippingStateID']);
        }
        $productCarts = $productCartTable->getByCartId($cartId);
        foreach($productCarts as $key=>$productCart){
            $productCarts[$key]['options'] = $productCartOptionTable->getByProductCartId($productCart['CartProductID']);
        }
        
        $session->getManager()->getStorage()->clear('hollywoodlace');
        return array('productCarts'=>$productCarts, 'cart'=>$cart);
    }
    
    private function _process($data)
    {
        $session = new Container('hollywoodlace');
        $cartId = $session->cartId;
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        
        $cartInfo = array('PayMethod'=>$data['paymentType'],'BillingFirstName'=>$data['billingFirstName'],'BillingLastName'=>$data['billingLastName'],
            'BillingCountryID'=>$data['billingCountry'],'BillingAddress'=>$data['billingAddress'],'BillingCity'=>$data['billingCity'],
            'BillingZipCode'=>$data['billingZip'],'BillingPhone'=>$data['billingPhone'],'BillingEmail'=>$data['billingEmail'],'BillingStateID'=>$data['billingState'],
            'BillingStateName'=>$data['billingState'],'ShippingFirstName'=>$data['shippingFirstName'],'ShippingLastName'=>$data['shippingLastName'],'ShippingCountryID'=>$data['shippingCountry'],
            'ShippingAddress'=>$data['shippingAddress'],'ShippingCity'=>$data['shippingCity'],'ShippingZipCode'=>$data['shippingZip'],
            'ShippingPhone'=>$data['shippingPhone'],'ShippingEmail'=>$data['shippingEmail'],'Referral'=>$data['referral'],'ShippingStateID'=>$data['shippingState'],
            'ShippingStateName'=>$data['shippingState']);
        
        $cart = $cartTable->select(array('CartID'=>$cartId))->current();            
        if(8 == $data['billingState'] && 8 != $cart['BillingStateID']){
            $cartInfo['Tax'] = $cart['PriceSubTotal']*6/100;
            $cartInfo['PriceTotal'] = $cart['PriceTotal'] + $cartInfo['Tax'];   
        }elseif(8 != $data['billingState'] && 8 == $cart['BillingStateID']){
            $cartInfo['PriceTotal'] = $cart['PriceTotal'] - $cart['Tax'];
            $cartInfo['Tax'] = 0;
            
        }
        
//        var_dump($cartInfo);die;
        $cartTable->save($cartInfo, $cartId);
    }
    
    private function _checkCartId()
    {
        $session = new Container('hollywoodlace');
        if($session->cartId)
            $this->_cartId = $session->cartId;
        else
            return $this->redirect()->toRoute('order', array());
    }
    
    private function _sentAdminMail($cart, $productCart){
        $this->renderer = $this->getServiceLocator()->get('ViewRenderer');
        $content = $this->renderer->render('application/cart/completepayment', array('cart'=>$cart, 'productCarts'=>$productCart));

        // make a header as html  
        $html = new \Zend\Mime\Part($content);
        $html->type = "text/html";
        $body = new \Zend\Mime\Message();
        $body->setParts(array($html));
        // instance mail   
        $mail = new \Zend\Mail\Message();
        $mail->setBody($body); // will generate our code html from template.phtml  
        $mail->setFrom(NotificationFrom);
        $mail->setBcc(NotificationToBcc);
        if ($cart['BillingEmail'] != 'andrey.petrunko@gmail.com') {
            $mail->addTo($cart['BillingEmail']);
            $mail->setSubject('(To Admin) New order on ' . ucfirst($_SERVER['SERVER_NAME']));
        } else {
            $mail->setSubject('New order on ' . ucfirst($_SERVER['SERVER_NAME']));
            $mail->addTo(NotificationTo);
        }
        
        $transporst = new \Zend\Mail\Transport\Sendmail();
        $transporst->send($mail);
    }
    
    private function _sentCustomerMail($cart, $productCart){
        $this->renderer = $this->getServiceLocator()->get('ViewRenderer');
        $content = $this->renderer->render('application/email/customer', array('cart'=>$cart, 'productCarts'=>$productCart));

        // make a header as html  
        $html = new \Zend\Mime\Part($content);
        $html->type = "text/html";
        $body = new \Zend\Mime\Message();
        $body->setParts(array($html));
        // instance mail   
        $mail = new \Zend\Mail\Message();
        $mail->setBody($body); // will generate our code html from template.phtml  
        $mail->setFrom(NotificationFrom);
        $mail->setBcc(NotificationToBcc);
        $mail->addTo($cart['BillingEmail']);
        $mail->setSubject('Order Confirmation on ' . ucfirst($_SERVER['SERVER_NAME']));
        
        $transporst = new \Zend\Mail\Transport\Sendmail();
        $transporst->send($mail);
    }
}
