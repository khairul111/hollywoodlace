<?php

namespace Application\Controller;

use Application\Controller\BaseController;
use Zend\View\Model\ViewModel;

class ProductController extends BaseController
{

    public function indexAction()
    {
        $productId = (int) $this->params()->fromRoute('id', 0);
        if (!$productId) {
            return $this->redirect()->toRoute('home');
        }
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $this->data['product'] = $productTable->getById($productId);
        $view = new ViewModel($this->data);
        return $view;
        
    }
    public function customhairAction()
    { 
        $categoryId = 7;
        $sm = $this->getServiceLocator();
        $categoryTable = $sm->get('Application\Model\CategoryTable');
        $productTable = $sm->get('Application\Model\ProductTable');
        $this->data['category'] = $categoryTable->getById($categoryId);
        $this->data['products'] = $productTable->getByCategoryId($categoryId);        
        $view = new ViewModel($this->data);
        return $view;
    }
    
    public function categoryAction()
    {
        $categoryId = (int) $this->params()->fromRoute('id', 0);
        if (!$categoryId) {
            return $this->redirect()->toRoute('static', array('action' => 'accessories'));
        }
        $sm = $this->getServiceLocator();
        $categoryTable = $sm->get('Application\Model\CategoryTable');
        $productTable = $sm->get('Application\Model\ProductTable');
        $this->data['category'] = $categoryTable->getById($categoryId);
        $this->data['products'] = $productTable->getByCategoryId($categoryId);        
        $view = new ViewModel($this->data);
        return $view;
        
    }
}
