<?php

namespace Application\Controller;

use Application\Controller\BaseController;
use Zend\View\Model\ViewModel;

class IndexController extends BaseController
{

    public function indexAction()
    {         
        $view = new ViewModel();
        return $view;
    }
    
}
