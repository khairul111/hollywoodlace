<?php

namespace Application\Controller;

use Application\Controller\BaseController;
use Application\Form\OrderForm;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class ReorderController extends BaseController
{

    public function indexAction()
    {
        $request = $this->getRequest();
        $viewModel = new \Zend\View\Model\ViewModel();
        if (!$request->isPost()) {           
            $viewModel->setTemplate('application/partial/login');
            return $viewModel;
        }
        $email = $request->getPost('email');
        $sm = $this->getServiceLocator();
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $cartTable = $sm->get('Application\Model\CartTable');
        $carts = $cartTable->getActiveByBillingEmail($email);
        if(!$carts){
            $viewModel->setTemplate('application/partial/login');
            return $viewModel;
        }
        
        foreach($carts as $key=>$cart){
            if($cart['Imported'] == 'Y'){
                $carts[$key]['products'] = $cartTable->prepareImportedOrderText($cart['OrderTXT'], true);
                continue;
            }
            $carts[$key]['products'] = $productCartTable->getByCartId($cart['CartID']);
            foreach($carts[$key]['products'] as $productKey =>$product){
                $carts[$key]['products'][$productKey]['options'] = $productCartOptionTable->getByProductCartId($product['CartProductID']);
            }
        }
        $viewModel->setVariables(array('carts'=>$carts));
        return $viewModel;
    }
    
    public function newAction()
    {
        $sm = $this->getServiceLocator();
        $productCartId = (int) $this->params()->fromRoute('id', 0);
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $productTable = $sm->get('Application\Model\ProductTable');
        
        $productCart = $productCartTable->getById($productCartId);
        $product = $productTable->getById($productCart['ProductID']);
        $optionGroups = $optionGroupTable->getByProductId($productCart['ProductID']);
        $optionGroupsValue = $this->prepareNewOrderOptionValue($productCart, $sm);
        $formDependency = array('optionGroups' => $optionGroups, 'productId' =>$productCart['ProductID'], 'categoryId'=>$productCart['CategoryID'],
            'optionValue'=>$optionGroupsValue,  'oldCartId'=>$productCart['CartID']);
        
        $orderForm = new OrderForm($sm, $formDependency);  
        $request = $this->getRequest();
        if ($request->isPost()) {
            $orderForm->setData($request->getPost());
            if ($orderForm->isValid()) {
                $cartId = $this->_process($orderForm->getData(), $productCart['ProductID']);
                return $this->redirect()->toRoute('order', array('action'=>'show'));
            }
        }
        $viewModel = new ViewModel(array('form'=>$orderForm, 'optionGroups'=>$optionGroups, 'product'=>$product));
        $viewModel->setTemplate('application/order/index');
        return $viewModel;
        
    }
    
    public function oldAction()
    {
        $param = explode('-', $this->params()->fromRoute('id', 0));
        $cartId = $param[0];
        $key = (array_key_exists('1', $param))?$param[1]:1;
        if(!$cartId){
            return $this->redirect()->toRoute('re-order', array('action'=>'index'));
        }
        $request = $this->getRequest();
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $productTable = $sm->get('Application\Model\ProductTable');
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        
        $cart= $cartTable->getById($cartId);
        $oldOrderInfo = $cartTable->prepareImportedOrderText($cart['OrderTXT'], true);
        $product = $productTable->getProductFromdecriptionText($oldOrderInfo[$key]['description']);
        
        $optionGroups = $optionGroupTable->getByProductId($product['ProductID']);
        $optionGroupsValue = $this->_prepareOldOrderOptionValue($oldOrderInfo[$key], $sm, $product['ProductID']);
        
        $formDependency = array('optionGroups' => $optionGroups, 'productId' =>$product['ProductID'], 'categoryId'=>$product['CategoryID'],
            'optionValue'=>$optionGroupsValue, 'oldCartId'=>$cartId);        
        $orderForm = new OrderForm($sm, $formDependency);  
        
        if ($request->isPost()) {
            $orderForm->setData($request->getPost());
            if ($orderForm->isValid()) {
                $data = $orderForm->getData();
                $cartId = $this->_process($orderForm->getData(), $product['ProductID']);
                $cartTable->save(array('PreviousOrderID'=>$data['oldCartId']), $cartId);
                return $this->redirect()->toRoute('order', array('action'=>'show'));
            }
        }
        
        if($product['CategoryID']!= 7 && $product['CategoryID']!= 8){
            return $this->redirect()->toRoute('order', array('action'=>'add', 'id'=>$product['ProductID'].'-'.$cartId));
        }
        $viewModel = new ViewModel(array('form'=>$orderForm, 'optionGroups'=>$optionGroups, 'product'=>$product));
        $viewModel->setTemplate('application/order/index');
        return $viewModel;  
    }
    
    private function prepareNewOrderOptionValue($productCart, $sm)
    {
        $optionValue = array();
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $options = $productCartOptionTable->getByProductCartId($productCart['CartProductID']);
        foreach($options as $option){
            $optionValue[$option['OptionGroupName']] = $option['OptionID'];
        }
        $optionValue['instruction'] = $productCart['AdditionalInstructions'];
        return $optionValue;
        
    }
    
    public function _prepareOldOrderOptionValue($orderDescription, $sm, $productId)
    {
        $descriptions = explode("<br>", $orderDescription['description']);
        $optionValue = array();
        $optionTable = $sm->get('Application\Model\OptionTable');
        foreach($descriptions as $row){
            $attribute = explode(':', $row);
            if(trim($attribute[0]) == 'ITEM NAME'){
                continue;
            }
            if('Instructions' == trim($attribute[0])){
                $attribute = explode('Instructions:', $orderDescription['description']);
                $optionValue['instruction'] = str_replace(array('<br>'), '', $attribute[1]);;
            }
            
            $similarOptionInfo = $optionTable->getByOldOption(trim($row));
            if(!$similarOptionInfo){
                continue;
            }
            
            $actualOption = $optionTable->getActualOption($similarOptionInfo['OptionName'], $productId);
            if(!$actualOption){
                continue;
            }
            $optionValue[trim($actualOption['OptionGroupName'])] = $actualOption['OptionID'];
            
        }
        
        $optionValue['Quantity'] = trim($orderDescription['quantity']).' unit';
        return $optionValue;
    }
    
    private function _process($data, $prevProductId)
    {
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $cartId = $this->_manageCart($cartTable, $data['oldCartId']);       
        $productCartId = $this->_manageProductCart($data, $cartId, $prevProductId);
        $cartTable->recalculateCart($cartId, $sm);
        return $cartId;
    }
    
    private function _manageCart($cartTable, $oldCartId = 0)
    {
        $session = new Container('hollywoodlace');
        if (isset($session->cartId) && intval($session->cartId) > 0) {
            $cartId = $session->cartId;
        }else{
            $cartId = $cartTable->save(array('SessionIPAddress'=>$_SERVER['REMOTE_ADDR'],'Added'=>date("Y-m-d H:i:s"),'Reorder'=>'Y','PreviousOrderID'=>$oldCartId));
            $session->cartId = $cartId;
        }
            
        return $cartId;
    }
    
    private function _manageProductCart($data, $cartId, $prevProductId)
    {
        
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $optionTable = $sm->get('Application\Model\OptionTable');
        
        $product = $productTable->getById($data['hairType']);
        $specialOffer = 'N';
        if(in_array('specialOffer', $data) && $data['specialOffer'] = 1){
            $specialOffer = 'Y';
        }
        if (isset($_REQUEST['Quantity']) && intval($_REQUEST['Quantity']) > 0) {
            $ProductQty = intval($_REQUEST['Quantity']);
        } else {
            $ProductQty = 1;
        }
          
        $productCartData = array('CartID'=>$cartId, 'ProductID'=>$product['ProductID'],'CategoryID'=>$product['CategoryID'],
            'CartProductName'=>$product['ProductName'], 'ProductPrice1'=>$product['ProductPrice1'], 'ProductPrice2'=>$product['ProductPrice2'],
            'ProductPrice3'=>$product['ProductPrice3'], 'HairCutPrice'=>$product['HairCutPrice'], 'FullCapPrice'=>$product['FullCapPrice'],
            'ChengFullCapPrice'=>$product['ChengFullCapPrice'], 'LyndFullCapPrice'=>$product['LyndFullCapPrice'], 'ProductPriceCheng'=>$product['ProductPriceCheng'],
            'ProductPriceLynd'=>$product['ProductPriceLynd'], 'ProductShippingPrice1'=>$product['ShippingPrice1'], 'ProductShippingPrice2'=>$product['ShippingPrice2'],
            'ProductQty'=>(in_array('quantity', $data))?$data['quantity']:1, 'SubTotalPrice'=>0, 'SubTotalShipping'=>0, 'TotalPrice'=>0,'LastUpdate'=>date("Y-m-d H:i:s"), 'IPAddress'=>$_SERVER['REMOTE_ADDR'],
            'ExtraPrice1'=>($data['ExtraPrice1'])?$data['ExtraPrice1']:0, 'ExtraPrice2'=>($data['ExtraPrice2'])?$data['ExtraPrice2']:0, 'SpecilOffer'=>$specialOffer,
            'ProfessionalStyle'=>($data['optional']==1)? 'Y':'N', 'FrontLength'=>$data['frontLength'], 'SideLength'=>$data['sideLength'],
            'BackLength'=>$data['backLength'], 'TopLength'=>$data['topLength'], 'AdditionalInstructions'=>$data['instruction'], 'PreviousCartID'=>$data['oldCartId']);
        
        
        if(array_key_exists('fullcap', $data)){
            $productCartData['FullCap'] = ($data['fullcap']== 1)?'Y':'N';
            for($i=1; $i<=7; $i++){
                $productCartData['Measurement'.$i] = $data['measurement'.$i];
            }
        }
        
        $productCart = $productCartTable->getByProductIdAndCartId($product['ProductID'], $cartId);
        if($productCart){
            $productCartTable->save($productCartData, $productCart['CartProductID']);
            $productCartId = $productCart['CartProductID'];
        }else{
            $productCartId = $productCartTable->save($productCartData);
            $optionGroups = $optionGroupTable->getByProductId($prevProductId);
            foreach($optionGroups as $optionGroup){
                $option = $optionTable->getById($data['GroupOptionID'.$optionGroup['OptionGroupID']]);
                if($option['OptionID'] == ''){
                    continue;
                }
                $cartProductOption = array('CartProductID'=>$productCartId,'OptionGroupID'=>$optionGroup['OptionGroupID'],'OptionGroupName'=>$optionGroup['OptionGroupName'],
                    'OptionGroupSortKey'=>$optionGroup['SortKey'],'OptionID'=>$option['OptionID'],'OptionName'=>$option['OptionName'],'AddPrice'=>$option['AddPrice'],
                    'PriceCheng'=>$option['PriceCheng'],'PriceLynd'=>$option['PriceLynd'],'LastUpdate'=>date("Y-m-d H:i:s"),'IPAddress'=>$_SERVER['REMOTE_ADDR']);

                $productCartUpdateData = array();
                if (strpos(strtolower('_' . $optionGroup['OptionGroupName']), "shipping") > 0) {
                    $productCartUpdateData = array('ProductShippingPrice1'=>$option['AddPrice'], 'ProductShippingPrice2'=>$option['AddPrice']);
                    $cartProductOption['AddPrice'] = 0;
                }

                if (strpos(strtolower('_' . $optionGroup['OptionGroupName']), "quantity") > 0) {
                    $productQuantity = intval(trim(substr($option['OptionName'], 0, 2)));
                    if($productQuantity > 1){
                        $productCartUpdateData['ProductQty'] = $productQuantity;
                    }
                }
                $productCartTable->save($productCartUpdateData, $productCartId);
                $productCartOptionId = $productCartOptionTable->save($cartProductOption);
            }
        }
        
        return $productCartId;
    }
    
    
}
