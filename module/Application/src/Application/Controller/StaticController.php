<?php

namespace Application\Controller;

use Application\Controller\BaseController;
use Zend\View\Model\ViewModel;

class StaticController extends BaseController
{

    public function supersilkAction()
    { 
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function superfineAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function thinskinAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function customhairAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function duplicatehairAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function stockhairAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function afroamericanAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function basicAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function accessoriesAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function haircolorAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function hairdensityAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function hairstyleAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function hairtypesAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function deliverypriorityAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function wavecurlAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function basesizingAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function aboutusAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function contactAction()
    { 
        $view = new ViewModel();
        return $view;
    }
    
    public function termsconditionAction()
    { 
        $view = new ViewModel();
        return array();
    }
    
    
    public function updategroupAction(){
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $products = $productTable->getByCategoryId(7);
        foreach($products as $product){
            $optionGroups = $optionGroupTable->getByProductId($product['ProductID']);
            foreach($optionGroups as $group){
                if(strtolower($group['OptionGroupName']) == 'shipping' && $group['OptionGroupID'] != 57){
                    $optionTable = $sm->get('Application\Model\OptionTable');
                    $options = $optionTable->getByGroupId($group['OptionGroupID']);
                    foreach($options as $option){
                        $optionTable->deleteItem($option['OptionID']);
                    }
                }
            }
            
            foreach($optionGroups as $group){
                if(strtolower($group['OptionGroupName']) == 'shipping' && $group['OptionGroupID'] != 57){
                    $optionTable = $sm->get('Application\Model\OptionTable');
                    $options = $optionTable->getByGroupId(57);
                    foreach($options as $option){
                        $newOption = $option;
                        $newOption['OptionID'] = null;
                        $newOption['GroupOptionID'] = $group['OptionGroupID'];
                        $optionTable->save($newOption);
                    }
                }
            }
        }
        
        die('hoye gese!');
    }
    
    public function updatestockAction(){
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $products = $productTable->getByCategoryId(8);
        foreach($products as $product){
            $optionGroups = $optionGroupTable->getByProductId($product['ProductID']);
            foreach($optionGroups as $group){
                if(strtolower($group['OptionGroupName']) == 'shipping' && $group['OptionGroupID'] != 274){
                    $optionTable = $sm->get('Application\Model\OptionTable');
                    $options = $optionTable->getByGroupId($group['OptionGroupID']);
                    foreach($options as $option){
                        $optionTable->deleteItem($option['OptionID']);
                    }
                }
            }
            
            foreach($optionGroups as $group){
                if(strtolower($group['OptionGroupName']) == 'shipping' && $group['OptionGroupID'] != 274){
                    $optionTable = $sm->get('Application\Model\OptionTable');
                    $options = $optionTable->getByGroupId(274);
                    foreach($options as $option){
                        $newOption = $option;
                        $newOption['OptionID'] = null;
                        $newOption['GroupOptionID'] = $group['OptionGroupID'];
                        $optionTable->save($newOption);
                    }
                }
            }
        }
        
        die('hoye gese!');
    }
}
