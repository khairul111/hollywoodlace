<?php

namespace Application\Controller;

use Application\Controller\BaseController;
use Zend\View\Model\ViewModel;

class FaqController extends BaseController {

    public function indexAction() {
        $view = new ViewModel();
        return $view;
    }
    
    public function generalAction() {
        $view = new ViewModel();
        $view->setTemplate('application/faq/index');
        return $view;
    }
    
    public function attachmentAction() {
        $view = new ViewModel();
        return $view;
    }
    
    public function fittingAction() {
        $view = new ViewModel();
        $view->setTemplate('application/static/basesizing');
        return $view;
    }
    
    public function systemcareAction() {
        $view = new ViewModel();
        return $view;
    }

}
