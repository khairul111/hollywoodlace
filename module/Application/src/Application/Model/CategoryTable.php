<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class CategoryTable extends AbstractTableGateway
{
    protected $table ='WebStoreProductCategories';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('CategoryID'=>$id));
        return $resultSet->current();
    }
    
    public function fetchAll($orderBy)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreProductCategories')
                ->order($orderBy);
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();        
    }
    
    public function save(array $category, $id=0){
        $category['LastUpdate'] = date('Y-m-d h:i:s');
        if ($id == 0) {
            $this->insert($cart);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->update($category, array('CategoryID' => $id));
            } else {
                throw new \Exception('Category  id does not exist');
            }
        }
    }
    
}

