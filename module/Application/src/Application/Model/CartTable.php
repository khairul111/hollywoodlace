<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class CartTable extends AbstractTableGateway
{
    protected $table ='WebStoreCart';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('CartID'=>$id));
        return $resultSet->current();
    }
    
    public function getWithStateCountryById($id)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('c'=>'WebStoreCart'))
                ->columns(array('BillingFirstName','BillingLastName', 'BillingEmail','BillingAddress','BillingCity', 'BillingZipCode',
                    'ShippingFirstName','ShippingLastName', 'ShippingAddress','ShippingCity', 'ShippingZipCode', 'ShippingPhone','PriceTotal'))
                ->join(array('s' => 'WebStoreStates'), 'c.BillingStateID=s.StateID', array('BillingStateCode'=>'StateCode'),'left')
                ->join(array('cn' => 'WebStoreCountries'), 'c.BillingCountryID=s.CountryID', array('BillingCountryCode'=>'CountryCode'),'left')
                ->join(array('sst' => 'WebStoreStates'), 'c.ShippingStateID=sst.StateID', array('ShippingStateCode'=>'StateCode'),'left')
                ->join(array('scn' => 'WebStoreCountries'), 'c.ShippingCountryID=s.CountryID', array('ShippitnCountryCode'=>'CountryCode'),'left')
                ->where('c.CartID = '.$id);
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->current();
    }
    public function  getAllLeads()
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreCart')
                ->columns(array('BillingFirstName','BillingLastName','BillingEmail','Added'))
                ->where("BillingFirstName<>'' AND BillingLastName<>'' AND PayMethod<>''");
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function getAllStateCustomer(){
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('c'=>'WebStoreCart'))
                ->columns(array('BillingFirstName','BillingLastName', 'BillingAddress','BillingCity', 'BillingZipCode', 'BillingPhoneCode',
                    'BillingPhone1', 'BillingPhone2','BillingPhone3', 'BillingPhoneExt','BillingEmail', 'Added'))
                ->join(array('s' => 'WebStoreStates'), 'c.BillingStateID=s.StateID', array('StateCode'))
                ->where("c.BillingFirstName<>'' AND c.BillingLastName<>''");
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function getAllStateCustomerBetweenDate($startDate, $endDate){
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('c'=>'WebStoreCart'))
                ->columns(array('BillingFirstName','BillingLastName', 'BillingAddress','BillingCity', 'BillingZipCode', 'BillingPhoneCode',
                    'BillingPhone1', 'BillingPhone2','BillingPhone3', 'BillingPhoneExt','BillingEmail', 'Added'))
                ->join(array('s' => 'WebStoreStates'), 'c.BillingStateID=s.StateID', array('StateCode'))
                ->where("c.BillingFirstName<>'' AND c.BillingLastName<>''  AND  c.Added>='$startDate' AND c.Added<='$endDate'");
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    public function  getActiveByBillingEmail($email)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreCart')
                ->where("BillingEmail = '$email' AND BillingFirstName<>'' AND BillingLastName<>'' AND CartStatusID>0")
                ->order('CartID desc');
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function  getAllBetweenDate($startDate, $endDate)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreCart')
                ->columns(array('CartID','Vendor', 'BillingFirstName', 'BillingLastName', 'PriceTotal','GrossProfit', 'Imported'))
                ->where("GrossProfit>0 AND Imported='N' AND BillingFirstName<>'' AND BillingLastName<>'' AND  Added>='$startDate' AND Added<='$endDate'")
                ->order('CartID desc');
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function  getByStateIdBetweenDate($stateId, $startDate, $endDate)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreCart')
                ->columns(array('CartID', 'Added', 'BillingFirstName','BillingLastName', 'BillingEmail', 'PriceTotal', 'PayMethod', 'Reorder', 'SampleReceived',
                    'CartStatusID', 'Vendor','SuppliesSent', 'VendorPriority', 'MessageToVendor', 'StockSystem', 'StockSent', 'OrderTXT2', 'Tax','Imported'))
                ->where("PayMethod<>'' AND BillingFirstName<>'' AND BillingLastName<>'' AND  Added>='$startDate' AND Added<='$endDate' AND BillingStateID=$stateId ")
                ->order('CartID desc');
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function fetchAll($orderBy)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreCart')
                ->columns(array('CartID', 'BillingFirstName', 'BillingLastName'))
                ->where("BillingFirstName<>'' AND BillingLastName<>''")
                ->order($orderBy);
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();        
    }
    
    public function save(array $cart, $id=0){
        if ($id == 0) {
            $this->insert($cart);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->update($cart, array('CartID' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
    public function deleteItem($id){
        return $this->delete('CartID ='. $id);
    }


    public function recalculateCart($cartId, $sm){
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        
        $cartPriceSubTotal = 0;
        $cartPriceSubTotalWithDiscount = 0;
        $cartPriceShipping = 0;
        $cartPriceTotal = 0;
        
        $productCarts = $productCartTable->getByCartId($cartId);
        foreach($productCarts as $productCart){
            
            $subTotalPrice = ($productCart['ProductPrice1'] + $productCart['ExtraPrice1'] + $productCart['ExtraPrice2']) * $productCart['ProductQty'] * $productCart['ProductQty2'];
            
            if ($productCart['CategoryID'] == 7 || $productCart['CategoryID'] == 8) {
                $shippingPriceTotal = $productCart['ProductShippingPrice1'];
            } else {
                $shippingPriceTotal = ($productCart['ProductShippingPrice1'] + ($productCart['ProductQty'] - 1) * $productCart['ProductShippingPrice2']) * $productCart['ProductQty2'];
            }
            $totalPrice = $subTotalPrice + $shippingPriceTotal;
            
            $additionalPrice = $productCartOptionTable->getPriceByProductCartId($productCart['CartProductID']);
            $subTotalPrice = $subTotalPrice + $additionalPrice * $productCart['ProductQty'] * $productCart['ProductQty2'];
            
            $productCartTable->save(array('SubTotalPrice'=>$subTotalPrice,'SubTotalShipping'=>$shippingPriceTotal,'TotalPrice'=>$totalPrice), $productCart['CartProductID']);
            
            $cartPriceSubTotal = $cartPriceSubTotal + $subTotalPrice;
            $cartPriceSubTotalWithDiscount = $cartPriceSubTotal;
            $cartPriceShipping = $cartPriceShipping + $shippingPriceTotal;
            $cartPriceTotal = $cartPriceSubTotalWithDiscount + $cartPriceShipping;
        }
        
        $cart = $this->getById($cartId);
        if ($cart['DiscountCode'] <> '' && ($cart['DiscountType'] == 'Percent' || $cart['DiscountType'] == 'Flat') && $cart['DiscountValue'] <> 0) {
            if ($cart['DiscountType'] == 'Percent') {
                $cartPriceSubTotalWithDiscount = $cartPriceSubTotalWithDiscount * (100 - $cart['DiscountValue']) / 100;
            } else {
                $cartPriceSubTotalWithDiscount = $cartPriceSubTotalWithDiscount - $cart['DiscountValue'];
            }

            $cartPriceTotal = $cartPriceSubTotalWithDiscount + $cartPriceShipping;
        }
        $updateArray = array('PriceSubTotal'=>$cartPriceSubTotal,'PriceSubTotalWithDiscount'=>$cartPriceSubTotalWithDiscount,
            'PriceShipping'=>$cartPriceShipping,'PriceTotal'=>$cartPriceTotal,'OrderHTML'=>'','Updated'=>date("Y-m-d H:i:s"));
        $this->save($updateArray, $cartId);
    }
    
    public function prepareImportedOrderText($orderText, $formAction = false){
        $items = explode('ITEM NAME', $orderText);
        $products = array();        
        foreach($items as $key=>$item){
            if(!$item){
                continue;
            }
            $itemText = 'ITEM NAME :'.$item;
            $itemNo = $key;
            if ($formAction) {
                $replaceble = array("AMOUNT $itemNo:", "QUANTITY $itemNo:","ON0 $itemNo:", "UID $itemNo: ", "ORDER ID: ");
            } else {
                $replaceble = array("AMOUNT $itemNo:", "QUANTITY $itemNo:", "SHIPPING $itemNo:", "SHIPPING UNITS $itemNo", "ON0 $itemNo:", "UID $itemNo: ",
                    "SHIPPING PRIORITY: ", "ORDER ID: ");
            }
            
            foreach($replaceble as $replaceText){
                $startPos = strpos($itemText,$replaceText);
                if($replaceText == "QUANTITY $itemNo:"){
                    $quantity = substr($itemText, $startPos+12, 2);
                }

                if($startPos > 0){
                    $endPos = strpos($itemText,"\n",$startPos+1);
                    if($endPos > $startPos)
                        $itemText = substr($itemText,0,$startPos).substr($itemText,$endPos+1);
                }
            }
            $itemText=str_replace("ITEM DESCRIPTION $key: ","",$itemText);
            $itemText=str_replace(", ","<br>",$itemText);
            $itemText=str_replace("AGREE TO TERMS: yes\r\n","",$itemText);
            $itemText=str_replace("OS0 $key: ","",$itemText);
            $itemText = str_replace("\r\n", "<br>", $itemText);
            $itemText = str_replace("<br>ITEM NAME 1:", "ITEM NAME 1:", $itemText);

            $itemText = str_replace('"', "&quot;", $itemText);
            $itemText = str_replace('Haircut_', "Haircut ", $itemText);
            $products[$key]['description'] = $itemText;
            $products[$key]['quantity'] = $quantity;
        }
        
        return $products;
    }
    
    public function getSummeryText($cartId, $sm){
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        $productCarts = $productCartTable->getByCartId($cartId);
        $summeryText = '';
        foreach($productCarts as $productCart){
            $summeryText .= "Item Name : ".$productCart['CartProductName'];
            $productCartOptions = $productCartOptionTable->getByProductCartId($productCart['CartProductID']);
            foreach($productCartOptions as $option){
                if($option['AddPrice'] <> 0){
                    $summeryText .= $option['OptionGroupName'].': '.$option['OptionName'].' (Add $' . number_format($option['AddPrice'], 2, '.', ',') . ')'."\n\r";
                }else{
                    $summeryText .= $option['OptionGroupName'].': '.$option['OptionName']."\n\r";
                }
            }
        }
        
        return $summeryText;
    }
    
    public function search($searchCriteria, $mainSearch = true){
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $where = "(NOT (Added>'2012-08-17 00:00:00' AND PayMethod='Credit Card' AND CartStatusID=0)) AND ((BillingFirstName<>'' AND BillingLastName<>'' AND PayMethod<>'') OR ((BillingFirstName<>'' OR BillingLastName<>'' OR BillingEmail<>'') AND Imported='Y'))";
        
        if($searchCriteria['searchDate'] == 'yes'){
            $startDate = date('Y-m-d', strtotime($searchCriteria['startDate'])).' 00:00:00';
            $endDate = date('Y-m-d', strtotime($searchCriteria['endDate'])).' 23:59:59';
            $where .= " AND Added>='$startDate' AND Added<='$endDate'";
        }
        
        if(array_key_exists('vendor', $searchCriteria))
            if($searchCriteria['vendor'] !='0'){$where .= " AND Vendor='$searchCriteria[vendor]'";}
            
        if($mainSearch){
            if($searchCriteria['firstName']<>''){$where .= " AND BillingFirstName LIKE '%$searchCriteria[firstName]%'";}
            if($searchCriteria['lastName']<>''){$where.=" AND BillingLastName LIKE '%$searchCriteria[lastName]%'";}
            if(intval($searchCriteria['orderNo'])>0){$where .=" AND CartID='$searchCriteria[orderNo]'";}
            if($searchCriteria['exportStatus']=='exported'){$where .= " AND (Exported='Y' OR ShippingCountryID>2)";}
            if($searchCriteria['exportStatus']=='nonExported'){$where .= " AND (Exported='N' AND (ShippingCountryID=1 OR ShippingCountryID=2))";}
            if($searchCriteria['displayMail'] == 1){$where .=" AND (OrderTXT LIKE '%Will Mail%' OR OrderTXT LIKE '%willmail%')";}
            if($searchCriteria['displayUnsentPaid'] == 1){$where .= " AND CartStatusID=1 AND MessageToVendor='' AND SuppliesSent<>'Y' AND StockSent<>'Y'";}
            if($searchCriteria['displayOnlyProduct'] == 1){$where .=" AND Imported='Y' AND OrderTXT NOT LIKE '%: Stock System%' AND OrderTXT NOT LIKE '%Super Fine%' AND OrderTXT NOT LIKE '%FRENCH%' AND OrderTXT NOT LIKE '%POLY%' AND OrderTXT NOT LIKE '%Swiss Lace%' AND OrderTXT NOT LIKE '%RULE:20+%' AND OrderTXT NOT LIKE '%RULE:45+%' AND OrderTXT NOT LIKE '%Priority: %'";}
            if($searchCriteria['suppliesSent'] == 1){$where .=" AND SuppliesSent='Y'";}
        }
        $select = $select->from(array('c'=>'WebStoreCart'))
                ->columns(array('CartID', 'Added', 'BillingFirstName','BillingLastName', 'BillingEmail', 'PriceTotal', 'PayMethod', 'Reorder','Imported',
                    'CartStatusID', 'Vendor','SuppliesSent', 'VendorPriority', 'MessageToVendor', 'StockSystem', 'StockSent', 'OrderTXT2', 'Tax', 'SampleReceived'))
                ->where($where)
                ->order('CartID desc');
        
        $selectString = $sql->getSqlStringForSqlObject($select);//die($selectString);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function searchForVendor($searchCriteria){
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $where = "BillingFirstName<>'' AND BillingLastName<>'' AND PayMethod<>''";
        
        if($searchCriteria['searchDate'] == 'yes'){
            $startDate = date('Y-m-d', strtotime($searchCriteria['startDate'])).' 00:00:00';
            $endDate = date('Y-m-d', strtotime($searchCriteria['endDate'])).' 23:59:59';
            $where .= " AND Added>='$startDate' AND Added<='$endDate'";
        }
        
        if(array_key_exists('vendor', $searchCriteria))
            if($searchCriteria['vendor'] !='0'){$where .= " AND Vendor='$searchCriteria[vendor]'";}
         
        $select = $select->from(array('c'=>'WebStoreCart'))
                ->columns(array('CartID', 'PriceTotal', 'PayMethod','BillingFirstName','BillingLastName', 'BillingAddress','BillingCity', 'BillingZipCode', 'BillingPhoneCode',
                    'BillingPhone1', 'BillingPhone2','BillingPhone3', 'BillingPhoneExt','BillingEmail', 'Added','OrderNotes','DiscountCode','ShippingMethod',
                    'OrderSummary','OrderNote','EstimatedDelivery','OrderTXT2','Reorder','PriceSubTotal','Tax','Vendor','Imported'))
                ->where($where)
                ->order('CartID desc');
        
        $selectString = $sql->getSqlStringForSqlObject($select);//die($selectString);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function prepareFinancialReport($searchData, $sm)
    {
        $startDate = (array_key_exists('startDate', $searchData))? date('Y-m-d', strtotime($searchData['startDate'])).' 00:00:00':date('Y-m-d h:i:s');
        $endDate = (array_key_exists('endDate', $searchData))? date('Y-m-d', strtotime($searchData['endDate'])).' 23:59:59':date('Y-m-d h:i:s');
        $carts = $this->getAllBetweenDate($startDate, $endDate);
        
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $productCartOptionTable = $sm->get('Application\Model\CartProductOptionTable');
        
        foreach($carts as $key=>$cart){
            $carts[$key]['accessoriesCount'] = $productCartTable->getAccessoriesCount($cart['CartID']);
            $carts[$key]['notAccessoriesCount'] = $productCartTable->getNonAccessoriesCount($cart['CartID']);
            $carts[$key]['prices'] = $productCartTable->getAllCartPrice($cart['CartID'], $sm, $carts[$key]);
            $carts[$key]['products'] = $productCartTable->getByCartId($cart['CartID']);
            
            foreach($carts[$key]['products'] as $productKey=>$product){
                $carts[$key]['products'][$productKey]['ProductPrice'] = $carts[$key]['products'][$productKey]['ProductPrice1'];
                $carts[$key]['products'][$productKey]['ProductCost'] = $carts[$key]['products'][$productKey]['ProductPriceLynd'];
                if($cart['Vendor'] == 'Cheng'){
                    $carts[$key]['products'][$productKey]['ProductCost'] = $carts[$key]['products'][$productKey]['ProductPriceCheng'];
                }
                $carts[$key]['products'][$productKey]['options'] = $productCartOptionTable->getByProductCartId($product['CartProductID']);
                
                foreach($carts[$key]['products'][$productKey]['options'] as $option){
                    if($option['OptionGroupName']=='Quantity'){
                        $carts[$key]['products'][$productKey]['ProductPrice'] = $option['AddPrice'];
                        $carts[$key]['products'][$productKey]['ProductCost'] = ($cart['Vendor'] == 'Cheng')?$option['PriceCheng']:$option['PriceLynd'];
                    }
                }
                
            }
        }
        
        return $carts;
    }
    
    public function reorder($cartId, $sm){
        $cart = $this->getById($cartId);
        if(!$cart){
            return false;
        }
        $cartArrayToInsert = array();
        foreach ($cart as $key => $value){
           if('CartID'==$key){
               continue;
           } 
           $cartArrayToInsert[$key]  = str_replace("'","\'",$value);
           if('Added'==$key || 'Updated'==$key){
               $cartArrayToInsert[$key]  = date("Y-m-d H:i:s");
           }
        }
        $newCartId = $this->save($cartArrayToInsert);
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $cartProducts = $productCartTable->getByCartId();
        foreach ($cartProducts as $product){
            $newCartProductoInsert = array();
            foreach ($product as $key => $value){
               if('CartProductID'==$key){
                   continue;
               } 
               $newCartProductoInsert[$key]  = str_replace("'","\'",$value);
               if('CartID'==$key){
                   $newCartProductoInsert[$key]  = $newCartId;
               }
               if('LastUpdate'==$key){
                   $newCartProductoInsert[$key]  = date("Y-m-d H:i:s");
               }
            }
            $productId = $productCartTable->save($cartArrayToInsert);
            
            $optionTable =  $sm->get('Application\Model\CartProductOptionTable');
            $productOptions = $optionTable->getByProductCartId($productId);
            foreach($productOptions as $option){
                $newProductoOptionInsert = array();
                foreach ($option as $key => $value){
                   if('OptionID'==$key){
                       continue;
                   }
                   $newProductoOptionInsert[$key]  = str_replace("'","\'",$value);
                   if('LastUpdate'==$key){
                       $newProductoOptionInsert[$key]  = date("Y-m-d H:i:s");
                   }
                }
                $optionTable->save($newProductoOptionInsert);
            }
        }
        
        return $newCartId;
    }
    
}
