<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class DiscountCodeTable extends AbstractTableGateway
{
    protected $table ='WebStoreDiscountCodes';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('DiscountCodeID'=>$id));
        return $resultSet->current();
    }
    
    public function fetchAll($orderBy)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreDiscountCodes')
                ->order($orderBy);
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();        
    }
    
    public function isValidCode($code)
    {
        $discountDate = date("Y-m-d");
        $resultSet = $this->select("Status='Active' AND DiscountCode='$code' AND StartDate<='$discountDate' AND EndDate>='$discountDate'");
        $discountCode = $resultSet->current();
        
        return $discountCode;
    }
    
    public function save(array $discountCode, $id=0){
        $discountCode['LastUpdate'] = date('Y-m-d h:i:s');
        if ($id == 0) {
            $this->insert($discountCode);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->update($discountCode, array('DiscountCodeID' => $id));
            } else {
                throw new \Exception('Category  id does not exist');
            }
        }
    }
    
    public function deleteItem($id){
        return $this->delete('DiscountCodeID ='. $id);
    }
}
