<?php

namespace Application\Model;

class Checkout {

    private $_sm;

    public function __construct($sm) {
        $this->_sm = $sm;
    }

    public function payWithCreditCard($data, $cartId,&$responseMessage) {
        $cartTable = $this->_sm->get('Application\Model\CartTable');
        if (strtolower($data['number']) == 'phone') {
            $cartTable->save(array('PayMethod' => 'Phone', 'CCNumber' => 'Phone'), $cartId);
            $this->payWithPaypal();
            return true;
        }

        $cart = $cartTable->getWithStateCountryById($cartId);

        $paymentType = urlencode('Sale');
        $amount = urlencode($cart['PriceTotal']);
        $expiry = $data['month'] . $data['year'];
        $IPAddress = $_SERVER['REMOTE_ADDR'];
        $shippingName = $cart['ShippingFirstName'] . ' ' . $cart['ShippingLastName'];
        $shippingPhone = urldecode($cart['ShippingPhone']);

        $nvpStr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$data[type]&ACCT=$data[number]" .
                "&EXPDATE=$expiry&CVV2=$data[code]&EMAIL=$cart[BillingEmail]&FIRSTNAME=$cart[BillingFirstName]&LASTNAME=$cart[BillingLastName]" .
                "&STREET=$cart[BillingAddress]&CITY=$cart[BillingCity]&STATE=$cart[BillingStateCode]&ZIP=$cart[BillingZipCode]" .
                "&COUNTRYCODE=$cart[BillingCountryCode]&CURRENCYCODE=USD&IPAddress=$IPAddress&SHIPTONAME=$shippingName&SHIPTOSTREET=$cart[ShippingAddress]" .
                "&SHIPTOSTREET2=&SHIPTOCITY=$cart[ShippingCity]&SHIPTOSTATE=$cart[ShippingStateCode]&SHIPTOZIP=$cart[ShippingZipCode]" .
                "&SHIPTOCOUNTRY=$cart[ShippitnCountryCode]&SHIPTOPHONENUM=$shippingPhone";

        $httpResponseArray = $this->paypalProHttpPost('DoDirectPayment', $nvpStr, 'sandbox', $responseMessage);
        if(!$httpResponseArray) return false;
        
        if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
            $gateWayTransactionID = $httpParsedResponseAr["TRANSACTIONID"];
            $lastUpdate = date("Y-m-d H:i:s");
            $this->cartOrderComplete($cartId);
            $updateArray = array('PayMethod' => 'Credit Card', 'CCTypeID' => $CCTypeID, 'CCNameOnCard' => $cart['BillingFirstName'] . ' ' . $cart['BillingLastName'],
                'CCNumber' => $data['number'], 'CCExpMonth' => $data['month'], 'CCExpYear' => $data['year'], 'CCCode' => $data['code'], 'GateWayDateTime' => $lastUpdate,
                'GateWayTransactionID' => $gateWayTransactionID);
            $cartTable->save($updateArray, $cartId);
            return $cartId;
        } else {
            $responseMessage = "This transaction cannot be proceed <br>".str_replace('%2e','',str_replace('%20','&nbsp;',$httpParsedResponseAr["L_LONGMESSAGE0"]));
            return false;
        }
    }

    function paypalProHttpPost($methodName_, $nvpStr, $environment, &$responseMessage) {

        $API_UserName = urlencode('willrichter_api1.gmail.com');
        $API_Password = urlencode('B938Q7MXQ6HRA7SJ');
        $API_Signature = urlencode('Ai9Ez9wvqYesEppN3jH5YMYODu2cAZRaYBTHd7ihTKTfC1Uai103LVrR');

        $API_Endpoint = "https://api-3t.paypal.com/nvp";
        if ("sandbox" === $environment || "beta-sandbox" === $environment) {
            $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
        }
        $version = urlencode('51.0');
        $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        // Turn off the server and peer verification (TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        // Set the request as a POST FIELD for curl.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

        // Get response from the server.
        $httpResponse = curl_exec($ch);
        if (!$httpResponse) {
            $responseMessage = $methodName_." failed: " . curl_error($ch) . '(' . curl_errno($ch) . ')';
            return false;
        }

        // Extract the response details.
        $httpResponseAr = explode("&", $httpResponse);

        $httpParsedResponseAr = array();
        foreach ($httpResponseAr as $i => $value) {
            $tmpAr = explode("=", $value);
            if (sizeof($tmpAr) > 1) {
                $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
            }
        }

        if ((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
            $responseMessage = "Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.";
            return $httpParsedResponseAr;
        }

        return $httpParsedResponseAr;
    }

    public function cartOrderComplete($cartId, $byPhone=false) {
        $cartTable = $this->_sm->get('Application\Model\CartTable');
        $cart = $cartTable->getById($cartId);

        if ($cart['Imported'] == 'Y' && strpos($cart['old_cart_details'], 'Stock System') > 0) {
            $stockUnitOrderMessage = $cart['old_cart_details'];
            $stockUnitOrderMessage = str_replace("<br>", CrLf, str_replace(CrLf, "", $stockUnitOrderMessage));
            $stockUnitOrderMessage = str_replace("ITEM NAME 1:", 'Order ID:' . $cartId . CrLf . "ITEM NAME 1:", $stockUnitOrderMessage);
            $this->prepareAndSendMail($cart['BillingEmail'], $stockUnitOrderMessage);
        }

        $productCartTable = $this->_sm->get('Application\Model\ProductCartTable');
        $productCart = $productCartTable->select(array('CategoryID' => 8, 'CartID' => $cartId))->current();
        $StockUnitOrderMessage = ($productCart) ? $this->_getOrderMessge($productCart) : null;

        if ($StockUnitOrderMessage) {
            $productCartOptionTable = $this->_sm->get('Application\Model\CartProductOptionTable');
            $option = $productCartOptionTable->select()->current(array('CartProductID' => $productCart['CartProductID'], 'OptionGroupName' => 'Hair Colors'));
            if ($option) {
                $StockUnitOrderMessage = str_replace("{color}", $option['OptionName'], $StockUnitOrderMessage);
                $this->prepareAndSendMail($cart['BillingEmail'], $StockUnitOrderMessage);
            }
        }

        $IPAddress = $_SERVER['REMOTE_ADDR'];
        $lastUpdate = date("Y-m-d H:i:s");
        if (!$byPhone) {
            $cartTable->save(array('GateWayTransactionStatusID' => 1, 'CartStatusID' => 1, 'CartStatusDateTime' => $lastUpdate,
                'CartStatusIPAddress' => $IPAddress), $cartId);
        }
        
    }

    private function _getOrderMessge($productCart) {
        $StockUnitOrderMessage = null;
        switch ($productCart['ProductID']) {
            case 46:
                $StockUnitOrderMessage = "Order ID: $productCart[CartID]
                    Item Name: Stock Unit Order
                    Super Fine Swiss Lace 
                    Color: {color}
                    Bleach or color cap front 2 inches
                    Gray: NoGray
                    Hairtype: RemyHumanHair
                    Hairdensity: 130
                    Hairtexture: Standard
                    Wavecurl: StraightBodyWave
                    Hairlength: 6in
                    Hairstyle: Free Style
                    Quantity: 1";
                break;
            case 47:
                $StockUnitOrderMessage = "Order ID: $productCart[CartID]
                    Item Name: Stock Unit Order
                    Super Fine Swiss Front 2 inches then french with side and back poly 
                    Color: {color}
                    Bleach or color cap front 2 inches
                    Gray: NoGray
                    Hairtype: RemyHumanHair
                    Hairdensity: 130
                    Hairtexture: Standard
                    Wavecurl: StraightBodyWave
                    Hairlength: 6in
                    Hairstyle: Free Style
                    Quantity: 1
                    Hairbasesize:  10 by 7 1/2 inches";
                break;
            case 48:
                $StockUnitOrderMessage = "Order ID: $productCart[CartID]
                    Item Name: Stock Unit Order
                    Super Fine Swiss Lace Front 2 inches then French with full poly perimeter 1/3 inches off front. front poly scalloped both sides
                    Color: {color}
                    Bleach or color cap front 1/3 inches
                    Gray: NoGray
                    Hairtype: RemyHumanHair
                    Hairdensity: 130
                    Hairtexture: Standard
                    Wavecurl: StraightBodyWave
                    Hairlength: 6in
                    Hairstyle: Free Style
                    Quantity: 1";
                break;
        }

        return $StockUnitOrderMessage;
    }

    public function prepareAndSendMail($to, $message) {
        $mail = new \Zend\Mail\Message();
        $mail->setBody($stockUnitOrderMessage);
        $mail->setFrom(StockUnitOrderEmailFrom);
        $mail->addBcc(StockUnitOrderEmailBcc);
        if ($cart['BillingEmail'] != 'andrey.petrunko@gmail.com') {
            $mail->addTo($cart['BillingEmail']);
            $mail->setSubject('(To Vendor) New Stock Hairpiece Order');
        } else {
            $mail->setSubject('New Stock Hairpiece Order');
            $mail->addTo(StockUnitOrderEmailTo);
        }
        $transporst = new \Zend\Mail\Transport\Sendmail();
        $transporst->send($mail);
    }

}
