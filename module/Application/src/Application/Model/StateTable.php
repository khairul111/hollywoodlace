<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class StateTable extends AbstractTableGateway
{
    protected $table ='WebStoreStates';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('StateID'=>$id));
        return $resultSet->current();
    }
    
    public function getNameById($id)
    {
        $resultSet = $this->select(array('StateID'=>$id));
        $state = $resultSet->current();
        
        return ($state)? $state['StateName']:false;
    }
    
    public function getByICountryd($id = 1)
    {
        $resultSet = $this->select(array('CountryID'=>$id));
        return $resultSet->toArray();
    }
    
    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
}
