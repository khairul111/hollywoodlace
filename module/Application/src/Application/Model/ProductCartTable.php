<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class ProductCartTable extends AbstractTableGateway
{
    protected $table ='WebStoreCartProducts';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('CartProductID'=>$id));
        return $resultSet->current();
    }
    
    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
    
    public function getByProductIdAndCartId($productId, $cartId)
    {
        $resultSet = $this->select(array('ProductID'=>$productId, 'CartID'=>$cartId));
        return $resultSet->current();
    }

    public function getByCartId($cartId)
    {
        $resultSet = $this->select(array('CartID'=>$cartId));
        return $resultSet->toArray();
    }
    
    public function getAccessoriesCount($cartId)
    {
        $resultSet = $this->select("CartID = $cartId AND CategoryID NOT IN (7,8)");
        return $resultSet->count();
    }
    
    public function getNonAccessoriesCount($cartId)
    {
        $resultSet = $this->select("CartID = $cartId AND CategoryID IN (7,8)");
        return $resultSet->count();
    }
    
    public function save(array $productCart, $id=0)
    {
        if(count($productCart)== 0){
            return false;
        }
        if ($id == 0) {
            $this->insert($productCart);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {                
                $this->update($productCart, array('CartProductID' => $id));
            } else {
                throw new \Exception('Data does not exist');
            }
        }
        
    }
    
    public function deleteItem($id)
    {
        $this->delete('CartProductID ='. $id);
    }
    
    public function getAllCartPrice($cartId, $sm, $cart){
        if('Y'==$cart['Imported']) return array('grossProfit'=>null, 'grossProfitPercent'=>null);
        
        $vendor = ($cart['Vendor'] == '' && $cart['notAccessoriesCount']==0 && $cart['accessoriesCount'] >0)?'Cheng':$cart['Vendor'];
        if($vendor != 'Cheng' && $vendor != 'Lynd'){
            return array('grossProfit'=>null, 'grossProfitPercent'=>null);
        }
        $optionTable = $sm->get('Application\Model\CartProductOptionTable');
        $totalPrice = 0;
        $subTotalPrice = 0;
        $products = $this->getByCartId($cartId);
        foreach($products as $product){
            $productPrice = ('Cheng'==$vendor)? $product['ProductPriceCheng']:$product['ProductPriceLynd'];
            $subTotalPrice = $subTotalPrice + $productPrice*$product['ProductQty'];
            $options = $optionTable->getByProductCartId($product['CartProductID']);
            foreach($options as $option){
                $optionPrice = ('Cheng'==$vendor)? $option['PriceCheng']:$option['PriceLynd'];
                $subTotalPrice = ($option['OptionGroupName']=='Shipping')? $subTotalPrice+$optionPrice: $subTotalPrice+$product['ProductQty']*$optionPrice;
            }
            $subTotalPrice = ($product['ProfessionalStyle']=='Y')? $subTotalPrice+18 : $subTotalPrice;
            if($product['FullCap'] == 'Y') {
                $MyFullCap = ($vendor == 'Cheng')?$product['ChengFullCapPrice']:$product['LyndFullCapPrice'];
                $subTotalPrice = $subTotalPrice + $MyFullCap;
            }
        }
        $grossProfit = ($cart['PriceTotal'] > $subTotalPrice)?$cart['PriceTotal']-$subTotalPrice:0;
        $grossProfitPercent=intval($grossProfit/$cart['PriceTotal']*100);
        return array('grossProfit'=>$grossProfit, 'grossProfitPercent'=>$grossProfitPercent);
    }
    
    public function getReorderInfo($cartId, $billingEmail, $cartProducts = null){
        if(!$cartProducts){
            return false;
        }
        $productIds = array();
        foreach($cartProducts as $product){
            $productIds[] = $product['ProductID'];
        }
        $productIds = implode(',', $productIds);
        
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('p'=>'WebStoreCartProducts'))
                ->columns(array('CartID'))
                ->join(array('c' => 'WebStoreCart'), 'p.CartID=c.CartID', array())
                ->where("p.CartID <> $cartId AND c.BillingEmail = '$billingEmail' AND ProductID IN($productIds)");
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE)->current();
        
        return ($results)? $results['CartID']:false;
    }
}
