<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class OptionGroupTable extends AbstractTableGateway
{
    protected $table ='WebStoreProductOptionGroups';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('OptionGroupID'=>$id));
        return $resultSet->current();
    }
    
    public function getByName($name)
    {
        $resultSet = $this->select("OptionGroupName like '%$name%'");
        return $resultSet->current();
    }
    
    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
    
    public function getByProductId($productId)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreProductOptionGroups')
                ->where(array('ProductID'=>$productId))
                ->order('SortKey, OptionGroupID');
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function getByCategory($categoryId)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('og'=>'WebStoreProductOptionGroups'))
                ->columns(array('OptionGroupID', 'OptionGroupName'))
                ->join(array('p' => 'WebStoreProducts'), 'p.ProductID=og.ProductID', array())
                ->where("p.CategoryID = $categoryId")
                ->order('og.SortKey, og.OptionGroupID');
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function save(array $optionGroup, $id=0){
        $optionGroup['LastUpdate'] = date('Y-m-d h:i:s');
        if ($id == 0) {
            $optionGroup['IPAddress'] = $_SERVER['REMOTE_ADDR']; 
            $this->insert($optionGroup);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->update($optionGroup, array('OptionGroupID' => $id));
            } else {
                throw new \Exception('Option Group id does not exist');
            }
        }
    }
    
    
    public function deleteItem($id){
        return $this->delete('OptionGroupID ='. $id);
    }
    
}
