<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class OptionTable extends AbstractTableGateway
{
    protected $table ='WebStoreProductOptions';
    protected $adapter;


    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('OptionID'=>$id));
        return $resultSet->current();
    }
    
    public function getOldById($oldOptionId)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreOldOptions')
                ->where("OptionId = $oldOptionId");
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->current();
    }
    public function getByName($name)
    {
        $resultSet = $this->select("OptionName like '%$name%'");
        return $resultSet->current();
    }
    
    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
    
    public function fetchAllOld()
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreOldOptions')
                ->order(array('SortKey', 'OptionID'));
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();        
    }
    
    public function getByGroupId($groupId)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreProductOptions')
                ->where(array('GroupOptionID'=>$groupId))
                ->order(array('SortKey','OptionID'));
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function getByOldOption($oldOptionName)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('op'=>'WebStoreProductOptions'))
                ->columns(array('OptionID','OptionName'))
                ->join(array('old' => 'WebStoreOldOptions'), 'old.OptionValueID=op.OptionID', array())
                ->where("OldOptionName like '%$oldOptionName'");
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->current();
    }
    
    public function getActualOption($optionName, $productId)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('op'=>'WebStoreProductOptions'))
                ->columns(array('OptionID','OptionName'))
                ->join(array('group' => 'WebStoreProductOptionGroups'), 'group.OptionGroupID = op.GroupOptionID', array('OptionGroupName'))
                ->where("op.OptionName = '$optionName' AND group.ProductID = $productId");
        $selectString = $sql->getSqlStringForSqlObject($select);
        
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->current();
    }
    
    public function getBySimilatGroup($groupId)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('op'=>'WebStoreProductOptions'))
                ->columns(array('OptionID', 'OptionName'))
                ->join(array('group1'=>'WebStoreProductOptionGroups'), 'group1.OptionGroupID = op.GroupOptionID', array())
                ->join(array('group2'=>'WebStoreProductOptionGroups'), 'group1.OptionGroupName = group2.OptionGroupName', array())
                ->where(array('group2.OptionGroupID'=>$groupId))
                ->order(array('op.SortKey','op.OptionID'))
                ->group('op.OptionName');
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }


    public function getAllOldOptions($sm)
    {
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $options = $this->fetchAllOld();
        foreach($options as $key=>$option){
            $options[$key]['optionGroups'] = $optionGroupTable->getByCategory($option['CategoryID']);
            $options[$key]['optionsInGroup'] = $this->getBySimilatGroup($option['GroupOptionID']);
        }
        
        return $options;
    }
    
    public function updateOld(array $option, $id=0){
        $option['LastUpdate'] = date('Y-m-d h:i:s');
        $sql = new Sql($this->adapter);
        $update = $sql->update();
        $update->table('WebStoreOldOptions');
        $update->set($option);
        $update->where(array('OptionID' => $id));
        $statement = $sql->prepareStatementForSqlObject($update);
        $statement->execute();
        return true;
    }
    
    public function save(array $optoin, $id=0){
        $optoin['LastUpdate'] = date('Y-m-d h:i:s');
        if ($id == 0) {
            $optoin['IPAddress'] = $_SERVER['REMOTE_ADDR']; 
            $this->insert($optoin);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->update($optoin, array('OptionID' => $id));
            } else {
                throw new \Exception('Option id does not exist');
            }
        }
    }
    
    public function deleteItem($id){
        return $this->delete('OptionID ='. $id);
    }
}
