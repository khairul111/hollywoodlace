<?php
namespace Application\Model;


class Reorder
{
    protected $inputFilter;
    
    public function prepareImportedOrderText($orderText)
    {
        $items = explode('ITEM NAME', $orderText);
        $products = array();        
        foreach($items as $key=>$item){
            if(!$item){
                continue;
            }
            $itemText = 'ITEM NAME :'.$item;
            $itemNo = $key;
            
            $replaceble = array("AMOUNT $itemNo:", "QUANTITY $itemNo:", "SHIPPING $itemNo:", "SHIPPING UNITS $itemNo", "ON0 $itemNo:", "UID $itemNo: ",
                "SHIPPING PRIORITY: ", "ORDER ID: ");
            foreach($replaceble as $replaceText){
                $startPos = strpos($itemText,$replaceText);
                if($replaceText == "QUANTITY $itemNo:"){
                    $quantity = substr($itemText, $startPos+12, 2);
                }

                if($startPos > 0){
                    $endPos = strpos($itemText,"\n",$startPos+1);
                    if($endPos > $startPos)
                        $itemText = substr($itemText,0,$startPos).substr($itemText,$endPos+1);
                }
            }
            $itemText=str_replace("ITEM DESCRIPTION $key: ","",$itemText);
            $itemText=str_replace(", ","<br>",$itemText);
            $itemText=str_replace("AGREE TO TERMS: yes\r\n","",$itemText);
            $itemText=str_replace("OS0 $key: ","",$itemText);
            $itemText = str_replace("\r\n", "<br>", $itemText);
            $itemText = str_replace("<br>ITEM NAME 1:", "ITEM NAME 1:", $itemText);

            $itemText = str_replace('"', "&quot;", $itemText);
            $itemText = str_replace('Haircut_', "Haircut ", $itemText);
            $products[$key]['description'] = $itemText;
            $products[$key]['quantity'] = $quantity;
        }
        
        return $products;
    }
}
