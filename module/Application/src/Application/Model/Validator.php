<?php
namespace Application\Model;

use Zend\InputFilter\Factory as InputFactory;     // <-- Add this import
use Zend\InputFilter\InputFilter;                 // <-- Add this import
use Zend\InputFilter\InputFilterAwareInterface;   // <-- Add this import
use Zend\InputFilter\InputFilterInterface;     

class Validator implements InputFilterAwareInterface
{
    protected $inputFilter;
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array('name'     => 'type','required' => true,)));
            $inputFilter->add($factory->createInput(array('name'     => 'number','required' => true,)));
            $inputFilter->add($factory->createInput(array('name'     => 'code','required' => true,)));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
