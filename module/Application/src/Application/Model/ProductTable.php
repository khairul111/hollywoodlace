<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class ProductTable extends AbstractTableGateway
{
    protected $table ='WebStoreProducts';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('ProductID'=>$id));
        return $resultSet->current();
    }
    
    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
    
    public function getByCategoryId($categoryId, $orderBy = 'SortKey')
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreProducts')
                ->where(array('CategoryID'=>$categoryId))
                ->order($orderBy);
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function getProductFromdecriptionText($orderDescription)
    {
        $descriptions = explode("<br>", $orderDescription);
        $product = null;
        foreach($descriptions as $row){
            $attribute = explode(':', $row);
            if(trim($attribute[0]) != 'ITEM NAME'){
                continue;
            }
            $productName = trim($attribute[count($attribute)-1]);
            $resultSet = $this->select("OldProductName like '%$productName%'");
            $product = $resultSet->current();
            break;
        }
        
        return $product;
        
    }
    
    public function save(array $product, $id=0){
        $product['LastUpdate'] = date('Y-m-d h:i:s');
        if ($id == 0) {
            $product['IPAddress'] = $_SERVER['REMOTE_ADDR']; 
            $this->insert($product);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->update($product, array('ProductID' => $id));
            } else {
                throw new \Exception('Category  id does not exist');
            }
        }
    }
    
    public function deleteItem($id){
        return $this->delete('ProductID ='. $id);
    }
    
    public function copy($oldProduct, $newData, $sm){ 
        $product = array();
        foreach($oldProduct as $key=>$value){
            if($key == 'ProductID') continue;
            $product[$key] = $value;
            
        }
        $product['ProductName'] = $newData['productName'];
        $product['SortKey'] = $newData['sortKey'];
        $newProductId = $this->save($product);
        $productTable = $sm->get('Application\Model\ProductTable');
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $optionTable = $sm->get('Application\Model\OptionTable');
        
        $optionGroups = $optionGroupTable->getByProductId($oldProduct['ProductID']);
        
        foreach ($optionGroups as $group){
            $newGroup = $group;
            $newGroup['ProductID'] = $newProductId;
            $newGroup['OptionGroupID'] = null;
            $newGroupId = $optionGroupTable->save($newGroup);
            $options = $optionTable->getByGroupId($group['OptionGroupID']);
            foreach ($options as $option){
                $newOption = $option;
                $newOption['OptionID'] = null;
                $newOption['GroupOptionID'] = $newGroupId;
                $optionTable->save($newOption);
            }
        }
        
    }
}
