<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class CountryTable extends AbstractTableGateway
{
    protected $table ='WebStoreCountries';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('CountryID'=>$id));
        return $resultSet->current();
    }
    
    public function getNameById($id)
    {
        $resultSet = $this->select(array('CountryID'=>$id));
        $country = $resultSet->current();
        
        return ($country)? $country['CountryName']:false;
    }
    
    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
}
