<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class CartProductOptionTable extends AbstractTableGateway
{
    protected $table ='WebStoreCartProductOptions';
    
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('OptionID'=>$id));
        return $resultSet->current();
    }
    
    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
    
    public function getByProductCartId($productCartId)
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreCartProductOptions')
                ->where(array('CartProductID'=>$productCartId))
                ->order(array('OptionGroupSortKey','ProductOptionID'));
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }


    public function  getPriceByProductCartId($productCartId)
    {
        $options = $this->select(array('CartProductID'=>$productCartId))->toArray();
        $price = 0;
        foreach ($options as $option){
            $price = $price + number_format($option['AddPrice'], 2);
        }
        
        return $price;
        
    }
    
    public function save(array $cartProductOption, $id = 0)
    {
        if ($id == 0) {
            return $this->insert($cartProductOption);
        } else {
            if ($this->getById($id)) {
                $this->update($data, array('ProductOptionID' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
}
