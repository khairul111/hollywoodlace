<?php

namespace Application\Form;

use Zend\Form\Form;
use Application\Model\ProductTable;

class CreditCardPaymentForm extends Form {

    public function __construct() {
        parent::__construct('creditCard');
        $this->_setTypeFields();
        $this->_setSubmit();
    }
    
    private function _setTypeFields()
    {
        $types = array('visa'=>'Visa', 'mastercard'=>'MasterCard', 'discover'=>'Discover', 'amex'=>'American Express');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'type',
            'options' => array('label' => 'Card Type','value_options' => $types),
            'attributes' => array('class'=>'field-medium'),
        ));
        
        $this->add(array('name' => 'number','attributes' => array('type' => 'text',),
            'options' => array('label' => 'Card No.',),
            'attributes'=>array('class'=>'field-medium', 'required'=>'required'),
        ));
        
        $months = array('01'=>'January', '02'=>'February', '03'=>'March', '04'=>'April', '05'=>'May', '06'=>'June',
            '07'=>'July', '08'=>'August', '09'=>'September', '10'=>'Octover', '11'=>'November', '12'=>'December',);
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'month',
            'options' => array('label' => 'Expiry Date','value_options' => $months),
            'attributes' => array('class'=>'field-small'),
        ));
        $years = array();
        for($year = date('Y'); $year<=date('Y')+10; $year++){ $years[$year] = $year;}
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'year',
            'options' => array('label' => 'Year','value_options' => $years),
            'attributes' => array('class'=>'field-small'),
        ));
        
        $this->add(array('name' => 'code','attributes' => array('type' => 'text',),
            'options' => array('label' => 'CVV2',),
            'attributes'=>array('class'=>'field-small', 'required'=>'required'),
        ));
    }

    private function _setSubmit() {
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Confirm and Pay order',
                'id' => 'submitbutton',
                'class'=>'button'
            ),
        ));
    }
}
