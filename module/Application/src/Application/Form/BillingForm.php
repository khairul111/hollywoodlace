<?php

namespace Application\Form;

use Zend\Form\Form;
use Application\Model\ProductTable;

class BillingForm extends Form {

    private $_sm;
    private $_attribute;
    private $_cart;


    public function __construct($sm,$formAttribute) {
        parent::__construct('billing');
        $this->_sm = $sm;
        $this->_attribute = $formAttribute;
        if(array_key_exists('paymentMethod', $this->_attribute)){
            $this->setAttribute('action', ($this->_attribute['paymentMethod'] == 'paypal')?'/cart/paypal':'');
        }
        $this->_setCart();
        $this->_setNameFields();
        $this->_setCountryFields();
        $this->_setStateFields();
        $this->_setAddressFields();
        $this->_setCityFields();
        $this->_setZipFields();
        $this->_setPhoneFields();
        $this->_setEmailFields();
        $this->_setOtherFields();
        $this->_setSubmit();
    }
    
    private function _setCart()
    {
        $this->_cart = false;
        if(array_key_exists('cart', $this->_attribute)){
            $this->_cart = $this->_attribute['cart'];
        }
    }
    
    private function _setNameFields()
    {
        $this->add(array('name' => 'billingFirstName',
                'attributes' => array(
                    'type' => 'text',
                ),
                'options' => array('label' => 'First Name',),
                'attributes'=>array('class'=>'field-medium first-name', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['BillingFirstName']:null),
            ));
        $this->add(array('name' => 'billingLastName',
                'attributes' => array(
                    'type' => 'text',
                ),
                'options' => array('label' => 'Last Name',),
                'attributes'=>array('class'=>'field-medium last-name', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['BillingLastName']:null),
            ));
        $this->add(array('name' => 'shippingFirstName',
                'attributes' => array(
                    'type' => 'text',
                ),
                'options' => array('label' => 'First Name',),
                'attributes'=>array('class'=>'field-medium  first-name', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['ShippingFirstName']:null),
            ));
        $this->add(array('name' => 'shippingLastName',
                'attributes' => array(
                    'type' => 'text',
                ),
                'options' => array('label' => 'First Name',),
                'attributes'=>array('class'=>'field-medium last-name', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['ShippingLastName']:null),
            ));
    }
    
    private function _setCountryFields()
    {
        $countries = $this->_sm->get('Application\Model\CountryTable')->fetchAll();
        $countryArray = array();
        foreach ($countries as $country) { 
            $countryArray[$country['CountryID']] = $country['CountryName'];
        }
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'billingCountry',
            'options' => array('label' => 'Country','value_options' => $countryArray,'class'=>'field-large'),
            'attributes' => array('class'=>'field-large country',
                'value'=>($this->_cart)?$this->_cart['BillingCountryID']:null),
        ));
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'shippingCountry',
            'options' => array('label' => 'Country','value_options' => $countryArray,'class'=>'field-large'),
            'attributes' => array('class'=>'field-large country',
                'value'=>($this->_cart)?$this->_cart['ShippingCountryID']:null),
        ));
    }

    private function _setStateFields()
    {
        $billingCountryId = ($this->_cart)?$this->_cart['BillingCountryID']:1;
        $billingStates = $this->_sm->get('Application\Model\StateTable')->getByICountryd($billingCountryId);
        $billingStateArray = array(0=>'Select billing state');
        foreach ($billingStates as $state) {
            $billingStateArray[$state['StateID']] = $state['StateName'];
        }
        
        $billingStateClasses = (count($billingStates) > 0)? 'field-medium state':'field-medium state hidden';
        $billingStateNameClasses = (count($billingStates) > 0)? 'field-medium hidden':'field-medium';
        
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'billingState','registerInArrayValidator' => false,
            'options' => array('label' => 'State','value_options' => $billingStateArray),
            'attributes' => array('class'=>$billingStateClasses,
                'value'=>($this->_cart)?$this->_cart['BillingStateID']:null),
        ));
        $this->add(array('name' => 'billingStateName',
            'attributes' => array('class' => $billingStateNameClasses,'type' => 'text',
                'value' => ($this->_cart) ? $this->_cart['BillingStateName'] : null),
        ));
        
        $shippingCountryId = ($this->_cart)?$this->_cart['ShippingCountryID']:1;
        $shippingStates = $this->_sm->get('Application\Model\StateTable')->getByICountryd($shippingCountryId);
        $shippingStateArray = array(0=>'Select shipping state');
        foreach ($shippingStates as $state) {
            $shippingStateArray[$state['StateID']] = $state['StateName'];
        }
        
        $shippingStateClasses = (count($shippingStates) > 0)? 'field-medium state':'field-medium state hidden';
        $shippingStateNameClasses = (count($shippingStates) > 0)? 'field-medium hidden':'field-medium';
        
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'shippingState', 'registerInArrayValidator' => false,
            'options' => array('label' => 'State','value_options' => $shippingStateArray),
            'attributes' => array('class'=>$shippingStateClasses,
                'value'=>($this->_cart)?$this->_cart['ShippingStateID']:null),
        ));
        
        $this->add(array('name' => 'shippingStateName',
            'attributes' => array('class' => $shippingStateNameClasses,'type' => 'text',
                'value' => ($this->_cart) ? $this->_cart['ShippingStateName'] : null),
        ));
    }
    
    private function _setAddressFields()
    {
        $this->add(array('name' => 'shippingAddress','attributes' => array('type' => 'text',),
                'options' => array('label' => 'Address',),
                'attributes'=>array('class'=>'field-medium address', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['ShippingAddress']:null),
            ));
        $this->add(array('name' => 'billingAddress','attributes' => array('type' => 'text',),
                'options' => array('label' => 'Address',),
                'attributes'=>array('class'=>'field-medium address', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['BillingAddress']:null),
            ));
    }
    
    private function _setCityFields()
    {
        $this->add(array('name' => 'shippingCity','attributes' => array('type' => 'text',),
                'options' => array('label' => 'City',),
                'attributes'=>array('class'=>'field-medium city', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['ShippingCity']:null),
            ));
        $this->add(array('name' => 'billingCity','attributes' => array('type' => 'text',),
                'options' => array('label' => 'City',),
                'attributes'=>array('class'=>'field-medium city', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['BillingCity']:null),
            ));
    }
    
    private function _setZipFields()
    {
        $this->add(array('name' => 'shippingZip','attributes' => array('type' => 'text',),
                'options' => array('label' => 'Zip',),
                'attributes'=>array('class'=>'field-small zip', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['ShippingZipCode']:null),
            ));
        $this->add(array('name' => 'billingZip','attributes' => array('type' => 'text',),
                'options' => array('label' => 'Zip',),
                'attributes'=>array('class'=>'field-small zip', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['BillingZipCode']:null),
            ));
    }
    
    private function _setPhoneFields()
    {
        $this->add(array('name' => 'shippingPhone','attributes' => array('type' => 'text',),
                'options' => array('label' => 'Phone',),
                'attributes'=>array('class'=>'field-medium phone', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['ShippingPhone']:null),
            ));
        $this->add(array('name' => 'billingPhone','attributes' => array('type' => 'text',),
                'options' => array('label' => 'Phone',),
                'attributes'=>array('class'=>'field-medium phone', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['BillingPhone']:null),
            ));
    }
    
    private function _setEmailFields()
    {
        $this->add(array('name' => 'shippingEmail','attributes' => array('type' => 'text',),
                'options' => array('label' => 'Email',),
                'attributes'=>array('class'=>'field-medium email', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['ShippingEmail']:null),
            ));
        $this->add(array('name' => 'billingEmail','attributes' => array('type' => 'text',),
                'options' => array('label' => 'Email',),
                'attributes'=>array('class'=>'field-medium email', 'required'=>'required',
                    'value'=>($this->_cart)?$this->_cart['ShippingEmail']:null),
            ));
    }
    
    private function _setSubmit() {
        if(!array_key_exists('paymentMethod', $this->_attribute)){
            return false;
        }
        $this->add(array(
            'name' => 'submit',
            'attributes' => array('type' => 'submit',
                'value' => ($this->_attribute['paymentMethod'] == 'creditCard')?'Proceed checkout with CreditCard':'Proceed checkout with Paypall',
                'id' => 'submitbutton',
                'class'=>'button'
            ),
        ));
    }
    
    private function _setOtherFields()
    {
        $options = array('Google'=>'Google', 'Yahoo'=>'Yahoo', 'Superpages'=>'SuperPages', 'Friend'=>'Friend', 'Other'=>'Other');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'referral',
            'options' => array('label' => 'Where did you hear about us','value_options' => $options),
            'attributes' => array('class'=>'field-medium'),
        ));
        if(array_key_exists('paymentMethod', $this->_attribute)){
            $this->add(array('name' => 'paymentType','attributes' => array(
                'type'  => 'hidden','value'=>  $this->_attribute['paymentMethod']),));
        }
        
    }
}
