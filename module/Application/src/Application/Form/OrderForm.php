<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter;
use Application\Model\ProductTable;

class OrderForm extends Form {

    private $_sm;
    private $_specialOfferText;
    private $_specialOfferId;
    private $_specialOfferQuantity;
    private $_optionValue = array();

    public function __construct($sm, $formDependency) {
        parent::__construct('order');
        $this->_sm = $sm;
        $this->_specialOfferQuantity = 2;
        
        $this->setAttributes(array('method'=>'post'));
        if(array_key_exists('optionValue', $formDependency)){
            $this->_optionValue = $formDependency['optionValue'];
        }
        $oldProductId = (array_key_exists('oldCartId', $formDependency))? $formDependency['oldCartId']:null;
        $this->_setHairType($formDependency['productId'], $formDependency['categoryId'], $oldProductId);
                
        foreach($formDependency['optionGroups'] as $optionGroup){
            $this->setHairOption($optionGroup, $formDependency['categoryId']);
        }
        if($formDependency['categoryId'] != 8) {
            $this->_setSpecialOption();
            $this->_setFullCapFields();
        }
        $this->_setOptionalFields();
        $this->_setAdditionalInstruction();
        $this->_setHiddenField($formDependency);
        $this->_setSubmit();
        $this->addInputFilter($formDependency['optionGroups']);
    }

    private function _setHairType($productId, $categoryId, $oldProductId = null) {
        $productTable = $this->_sm->get('Application\Model\ProductTable');
        $products = $productTable->getByCategoryId($categoryId);
        $productArray = array('0'=>'Select Hair Type');
        foreach ($products as $key => $product) {
            $productArray[$product['ProductID']] = $product['ProductName'];
        }
        
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'hairType',
            'options' => array('label' => 'Hair Type','value_options' => $productArray),
            'attributes' => array('value'=>$productId,'class'=>($oldProductId)?'field-large':'field-large hair-type'),
        ));
    }

    public function setHairOption(array $optionGroup, $categoryId) {
        $optionTable = $this->_sm->get('Application\Model\OptionTable');
        $options = $optionTable->getByGroupId($optionGroup['OptionGroupID']);
        $optionGroupName = $optionGroup['OptionGroupName'];
        
        $hasGroup = (array_key_exists($optionGroupName, $this->_optionValue))? true:false;
        $defaultValue = ($hasGroup)?$this->_optionValue[$optionGroupName]:0;
        if($optionGroupName == 'Quantity'){
            $this->_setSpecialOfferInfo($options);
        }
        $optionArray = ($categoryId==8)?array():array(null=>'Select');
        foreach ($options as $option) {
            $optionName = $option['OptionName'];
            if($optionGroupName != 'Quantity' && $option['AddPrice'] <> 0){
                $optionName = $option['OptionName'].' (Add $' . number_format($option['AddPrice'], 2, '.', ',') . ')';
            }
            
            if($optionGroupName == 'Quantity' && $hasGroup){
                $defaultValue = (strpos('-' . $option['OptionName'], $this->_optionValue[$optionGroupName]))?$option['OptionID']:$defaultValue;
            }
            $optionArray[$option['OptionID']] = $optionName;
            
        }
        if(trim(strtolower($optionGroup['OptionGroupName'])) == 'priority') $optionGroup['OptionGroupName'] = 'Production Time';
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'GroupOptionID' . $optionGroup['OptionGroupID'],
            'options' => array('label' => $optionGroup['OptionGroupName'],'value_options' => $optionArray),
            'attributes' => array('class'=>'field-medium', 'required'=>true, 'id'=>  strtolower($optionGroup['OptionGroupName']),
                'value'=>$defaultValue),
        ));
    }

    private function _setSpecialOption() {
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'specialOffer',
            'options' => array(
                'label' => "<b>Special Offer</b> ".$this->_specialOfferText,
                'checked_value' => $this->_specialOfferId,
                'unchecked_value' => 0
            ),
            'attributes'=>array('id'=>'special-offer'),
        ));
    }

    private function _setOptionalFields() {
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'optional',
            'options' => array(
                'label' => '(Optional)'
            ),
            'attributes'=>array('id'=>'optional', 'class'=>'show-hide-fields'),
        ));

        $fronthLengthArray = array('1' => '1 inch', '1 1/2' => '1 1/2 inch', '2' => '2 inch',
            '2 1/2' => '2 1/2 inch', '3' => '3 inch', '3 1/2' => '3 1/2 inch',
            '4' => '4 inch', 'Copy sample pice' => 'Copy sample pice', 'Others' => 'Others');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'frontLength',
            'options' => array(
                'label' => 'Front Length',
                'value_options' => $fronthLengthArray
            ),
            'attributes' => array('class'=>'field-medium'),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'sideLength',
            'options' => array(
                'label' => 'Side Length',
                'value_options' => $fronthLengthArray
            ),
            'attributes' => array('class'=>'field-medium'),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'backLength',
            'options' => array(
                'label' => 'Back Length',
                'value_options' => $fronthLengthArray
            ),
            'attributes' => array('class'=>'field-medium'),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'topLength',
            'options' => array(
                'label' => 'Top Length',
                'value_options' => $fronthLengthArray
            ),
            'attributes' => array('class'=>'field-medium'),
        ));
    }

    private function _setFullCapFields() {
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'fullcap',
            'options' => array(
                'label' => '(Full cap)'
            ),
            'attributes'=>array('id'=>'fullcap', 'class'=>'show-hide-fields'),
        ));
        for($i = 1; $i<=7; $i++){
            $this->add(array(
                'name' => 'measurement'.$i,
                'attributes' => array(
                    'type' => 'text',
                ),
                'options' => array(
                    'label' => 'Measurement '.$i,
                ),
                'attributes'=>array('class'=>'field-small'),
            ));
        }
        
    }

    private function _setAdditionalInstruction() {
        $default = null;
        if(array_key_exists('instruction', $this->_optionValue)){
            $default = trim($this->_optionValue['instruction']);
        }
        $this->add(array('name' => 'instruction','attributes' => array('type' => 'textarea',),
            'options' => array('label' => 'Add Additional Instructions In Box Below(optional)',),
            'attributes'=>array('class'=>'field-large field-height-medium', 'value'=>$default)
        ));
    }
    
    private function _setSubmit() {
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
    
    private function _setHiddenField($formDependency)
    {
        $this->add(array('name' => 'ExtraPrice1','attributes' => array('type'  => 'hidden','value'=>0),));
        $this->add(array('name' => 'ExtraPrice2','attributes' => array('type'  => 'hidden','value'=>0),));
        $this->add(array('name' => 'oldCartId','attributes' => array('type'  => 'hidden'
            ,'value'=>(array_key_exists('oldCartId', $formDependency)?$formDependency['oldCartId']:null))));
    }
    
    private function _setSpecialOfferInfo($options)
    {
        foreach($options as $key=>$option){
            if(($key +1)==$this->_specialOfferQuantity){
                $this->_specialOfferText = 'Check here to receive '.$this->_specialOfferQuantity.' systems for '.$option['AddPrice'].' each';
                $this->_specialOfferId = $option['OptionID'];
                break;
            }
        }
    }
    
    public function addInputFilter($optionGroups)
    {
        $inputFilter = new InputFilter\InputFilter();
        
        foreach($optionGroups as $optionGroup){
            $inputField = new InputFilter\Input('GroupOptionID' . $optionGroup['OptionGroupID']);
            $inputField->setRequired(true);      
            $inputFilter->add($inputField);
        }
        
        $this->setInputFilter($inputFilter);
    }
}
