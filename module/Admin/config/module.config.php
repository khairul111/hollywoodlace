<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Order' => 'Admin\Controller\OrderController',
            'Admin\Controller\Auth' => 'Admin\Controller\AuthController',
            'Admin\Controller\Store' => 'Admin\Controller\StoreController',
            'Admin\Controller\Account' => 'Admin\Controller\AccountController',
            'Admin\Controller\Report' => 'Admin\Controller\ReportController',
            'Admin\Controller\Maintenance' => 'Admin\Controller\MaintenanceController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'admin' => __DIR__ . '/../view',
        ),
        
    ),
    'router' => array(
        'routes' => array(
            'admin-home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/admin',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Order',
                        'action'     => 'index',
                    ),
                ),
            ),
            'admin-order' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order[/:action][/:id][/:orderby]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Order',
                        'action' => 'index',
                    ),
                ),
            ),
            'admin-store' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/store[/:action][/:id][/:orderby]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Store'
                    ),
                ),
            ),
            'admin-account' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/account[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Account',
                        'action' => 'user'
                    ),
                ),
            ),
            'admin-auth' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/auth[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Auth',
                        'action' => 'login',
                    ),
                ),
            ),
            'admin-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/report[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Report'
                    ),
                ),
            ),
            'admin-maintenance' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/maintenance[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Maintenance'
                    ),
                ),
            ),
        )
    ),
    'module_layouts' => array(
        'Admin' => 'layout/layout.phtml',
    ),
);