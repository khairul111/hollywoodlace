<?php
namespace Admin;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Admin\Model\AclResourceTable;
use Admin\Model\FixedCostTable;
use Admin\Model\AccountTable;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
//            'Zend\Loader\ClassMapAutoloader' => array(
//                __DIR__ . '/autoload_classmap.php',
//            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function onBootstrap($e)
    {
            $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
                    $controller = $e->getTarget();
                    $controllerClass = get_class($controller);
                    $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
                    $config = $e->getApplication()->getServiceManager()->get('config');
                    if (isset($config['module_layouts'][$moduleNamespace])) {
                        $controller->layout($config['module_layouts'][$moduleNamespace]);
                    }
                }, 100);
                
//        define('NotificationFrom','hollywoodlaceorders@yahoo.com');
//        define('NotificationTo','hollywoodlaceorders@yahoo.com');
        
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories'=>array(
		'AuthService' => function($sm) {
                    $dbAdapter           = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter  = new DbTableAuthAdapter($dbAdapter, 
                                              'WebStoreAccounts','UsrLogin','UsrPassword');
		    
		    $authService = new AuthenticationService();
		    $authService->setAdapter($dbTableAuthAdapter);
		     
		    return $authService;
		},
                'Admin\Model\AccountTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new AccountTable($dbAdapter);
                    return $table;
                },
                'Admin\Model\AclResourceTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new AclResourceTable($dbAdapter);
                    return $table;
                },
                'Admin\Model\FixedCostTable' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new FixedCostTable($dbAdapter);
                    return $table;
                }
            ),
        );
    }
    
    public function loadCommonViewVars(MvcEvent $e)
    {
        $e->getViewModel()->setVariables(array(
            'loginbtn' => ($e->getApplication()
                            ->getServiceManager()->get('AuthService')->hasIdentity()) ? 'Go to Profile' : 'Login'
        )); 
    }

}