<?php

namespace Admin\Form;

use Zend\Form\Form;

class LoginForm extends Form {

    public function __construct() {
        parent::__construct('logic');
        $this->_setTypeFields();
        $this->_setSubmit();
    }
    
    private function _setTypeFields()
    {
        $this->add(array('name' => 'username','attributes' => array('type' => 'text'),
                'options' => array('label' => 'UserName')));
        
        $this->add(array('name' => 'password','attributes' => array('type' => 'password'),
                'options' => array('label' => 'Password')));
        
        
    }

    private function _setSubmit() {
        $this->add(array('name' => 'submit',
            'attributes' => array('type' => 'submit','value' => 'Confirm and Pay order','id' => 'submitbutton','class'=>'button'),
        ));
    }
}
