<?php

namespace Admin\Form;

use Zend\Form\Form;

class DiscountCodeForm extends Form {

    public $discountCode;
    public function __construct($discountCode = array()) {
        parent::__construct('discount-code');
        $this->discountCode = $discountCode;
        $this->_setFields();
    }
    
    private function _setFields()
    {
        $this->add(array('name' => 'DiscountTitle','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=> (array_key_exists('DiscountTitle', $this->discountCode))?$this->discountCode['DiscountTitle']:''),
                'options' => array('label' => ' Title')));
        
        $this->add(array('name' => 'DiscountCode','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('DiscountCode', $this->discountCode))?$this->discountCode['DiscountCode']:''),
                'options' => array('label' => 'Discount Code')));
        
        $this->add(array('name' => 'OrderMinimum','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('OrderMinimum', $this->discountCode))?$this->discountCode['DiscountCode']:''),
                'options' => array('label' => 'Order Minimum')));
        
        $discountType = array('Percent'=>'Percent','Flat'=>'Flat');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'DiscountType',
            'options' => array('label' => 'Discount Type','value_options' => $discountType),'attributes' => array('value'=>(array_key_exists('DiscountType', $this->discountCode))?$this->discountCode['DiscountType']:''),));
        
        $this->add(array('name' => 'DiscountValue','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('DiscountValue', $this->discountCode))?$this->discountCode['DiscountValue']:''),
                'options' => array('label' => 'Discount')));
        
        $this->add(array('name' => 'StartDate','attributes' => array('type' => 'text', 'class'=>'text short datepicker', 'value'=>  (array_key_exists('StartDate', $this->discountCode))?date('m/d/Y', strtotime($this->discountCode['StartDate'])):''),
                'options' => array('label' => 'Start Date')));
        
        $this->add(array('name' => 'EndDate','attributes' => array('type' => 'text', 'class'=>'text short datepicker', 'value'=>  (array_key_exists('EndDate', $this->discountCode))?date('m/d/Y', strtotime($this->discountCode['EndDate'])):''),
                'options' => array('label' => 'End Date')));
        
        $this->add(array('name' => 'Description','attributes' => array('type' => 'textarea', 'class'=>'text large','value'=> (array_key_exists('Description', $this->discountCode))?$this->discountCode['Description']:'', 'rows'=>10, 'cols'=>80),
                'options' => array('label' => 'Description')));
        
        $discountType = array('Active'=>'Active','Disabled'=>'Disabled');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'Status',
            'options' => array('label' => 'Exported','value_options' => $discountType),'attributes' => array((array_key_exists('Status', $this->discountCode))?$this->discountCode['Status']:''),));
        
        
    }

}
