<?php

namespace Admin\Form;

use Zend\Form\Form;

class userForm extends Form {

    public $user;
    private $sm;
    public function __construct($user, $sm) {
        parent::__construct('user');
        $this->user = $user;
        $this->sm = $sm;
        $this->_setFields();
        $this->_setAccess();
    }
    
    private function _setFields()
    {
        $this->add(array('name' => 'UsrLogin','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=> array_key_exists('UsrLogin', $this->user)? $this->user['UsrLogin']:null),
                'options' => array('label' => ' Login')));
        $this->add(array('name' => 'UsrPassword','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=> array_key_exists('UsrPassword', $this->user)? $this->user['UsrPassword']:null),
                'options' => array('label' => 'Password')));
        $this->add(array('name' => 'FirstName','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=> array_key_exists('FirstName', $this->user)? $this->user['FirstName']:null),
                'options' => array('label' => ' First Name')));
        $this->add(array('name' => 'LastName','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=> array_key_exists('LastName', $this->user)? $this->user['LastName']:null),
                'options' => array('label' => 'Last Name')));
    }
    
    private function _setAccess(){
        $resourceTable = $this->sm->get('Admin\Model\AclResourceTable');
        $resources = $resourceTable->fetchAll();
        $aclResources = array();
        $defaultValue = array();
        foreach($resources as $resource){
            $aclResources[$resource['PageID']] = $resource['PageName'];
        }
        if(array_key_exists('AccessLevel', $this->user)){
            $defaultValue = explode(',', $this->user['AccessLevel']);
        }
        $this->add(array('type' => 'Zend\Form\Element\MultiCheckbox','name' => 'resources',
            'options' => array('label' => "Access Level",'value_options' =>$aclResources),
            'attributes'=>array('value'=>$defaultValue),
        ));
        
    }

}
