<?php

namespace Admin\Form;

use Zend\Form\Form;

class OldProductForm extends Form {

    public $product;
    public function __construct($product = null) {
        parent::__construct('old-product');
        $this->product = ($product)?$product:array();
        $this->_setFields();
    }
    
    private function _setFields()
    {        
        $this->add(array('name' => 'OldProductName','attributes' => array('type' => 'text', 'class'=>'text long', 'value'=> (array_key_exists('OldProductName', $this->product))?$this->product['OldProductName']:''),
                'options' => array('label' => 'Old Product Names')));        
    }

}
