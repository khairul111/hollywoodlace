<?php

namespace Admin\Form;

use Zend\Form\Form;

class OptionForm extends Form {

    public $product;
    public function __construct($option = null) {
        parent::__construct('option');
        $this->option = ($option)?$option:array();
        $this->_setFields();
    }
    
    private function _setFields()
    {
        
        $this->add(array('name' => 'GroupOptionID','attributes' => array('type' => 'hidden', 'value'=> (array_key_exists('GroupOptionID', $this->option))?$this->option['GroupOptionID']:'')));
        
        $this->add(array('name' => 'OptionName','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=> (array_key_exists('OptionName', $this->option))?$this->option['OptionName']:''),
                'options' => array('label' => 'Value of Option')));
                
        $this->add(array('name' => 'AddPrice','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('AddPrice', $this->option))?$this->option['AddPrice']:''),
                'options' => array('label' => 'Add Price')));
        
        $this->add(array('name' => 'PriceCheng','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('PriceCheng', $this->option))?$this->option['PriceCheng']:''),
                'options' => array('label' => 'Cheng Price')));
        
        $this->add(array('name' => 'PriceLynd','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('PriceLynd', $this->option))?$this->option['PriceLynd']:''),
                'options' => array('label' => 'Lynd Price')));
        
        $ByDefault = array('N'=>'No','Y'=>'Yes');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'ByDefault',
            'options' => array('label' => 'By Default','value_options' => $ByDefault),'attributes' => array('class'=>'field-small',
                'value'=>(array_key_exists('ByDefault', $this->option))?$this->option['ByDefault']:'N'),
        ));
        
        $this->add(array('name' => 'SortKey','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=> (array_key_exists('SortKey', $this->option))?$this->option['SortKey']:''),
                'options' => array('label' => 'SortKey')));
        
    }

}
