<?php

namespace Admin\Form;

use Zend\Form\Form;
use Application\Form\BillingForm;

class OrderForm extends Form {
    
    private $cart;

    public function __construct($cart) {
        parent::__construct('Order');
        $this->cart = $cart;
        $this->_setBasicInfoFields();
        $this->_setCardInfo();
        $this->_setFulfilmentInfo();
        
    }
    
    private function _setBasicInfoFields()
    {
        $this->add(array('name' => 'firstName','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>($this->cart['FirstName']=='')?$this->cart['BillingFirstName']:$this->cart['FirstName']),
                'options' => array('label' => 'First Name')));
        
        $this->add(array('name' => 'lastName','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>($this->cart['LastName']=='')?$this->cart['BillingLastName']:$this->cart['LastName']),
                'options' => array('label' => 'Last Name')));
        
        $this->add(array('name' => 'orderDate','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['Added']),
                'options' => array('label' => 'Order Date')));
        
        $this->add(array('name' => 'orderNotes','attributes' => array('type' => 'textarea','class'=>'long', 'value'=>  $this->cart['OrderNotes'], 'rows'=>5),
            'options' => array('label' => 'Order Notes',)
        ));
        
        $this->add(array('name' => 'discountCode','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['DiscountCode']),
                'options' => array('label' => 'Discount Code')));
        
        $this->add(array('name' => 'shippingMethod','attributes' => array('type' => 'text', 'class'=>'text long', 'value'=>  $this->cart['ShippingMethod']),
                'options' => array('label' => 'Shipping Method')));
        
        $this->add(array('name' => 'referral','attributes' => array('type' => 'text', 'class'=>'text long', 'value'=>  $this->cart['Referral']),
                'options' => array('label' => 'Referral')));
        
        $this->add(array('name' => 'summary','attributes' => array('type' => 'textarea','class'=>'long', 'value'=>  $this->cart['OrderSummary'], 'rows'=>5),
            'options' => array('label' => 'Order Summary',)
        ));
        
        $cartStatus = array('0'=>'Not Completed', '1'=>'Paid, Not Shipped', '2'=>'Paid and Shipped', '3'=>'Cancelled');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'status',
            'options' => array('label' => 'Order Status','value_options' => $cartStatus),'attributes' => array('class'=>'field-medium','value'=>$this->cart['CartStatusID']),
        ));
        
        $this->add(array('name' => 'note','attributes' => array('type' => 'text', 'class'=>'text long', 'value'=>  $this->cart['OrderNote']),
                'options' => array('label' => 'Order Note')));
        
        $this->add(array('name' => 'deliveryDate','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['EstimatedDelivery']),
                'options' => array('label' => 'Estimated Delivery')));
        
        $this->add(array('name' => 'details','attributes' => array('type' => 'textarea','class'=>'long', 'value'=>  $this->cart['OrderTXT2'], 'rows'=>15),
            'options' => array('label' => 'Order Details',)
        ));
        
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'reorder',
             'options' => array('label' => 'Reorder','value_options' => array('Y' => 'Yes','N' => 'No',),'attributes'=>array('value'=>$this->cart['Reorder']))
        ));
        
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'stockSystem',
             'options' => array('label' => 'Stock System','value_options' => array('Y' => 'Yes','N' => 'No',),'attributes'=>array('value'=>$this->cart['StockSystem']))
        ));
        
        $this->add(array('name' => 'flag','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['Flags']),
                'options' => array('label' => 'Flags')));
        
        $this->add(array('name' => 'subTotal','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['PriceSubTotal']),
                'options' => array('label' => 'SubTotal')));
        
        $this->add(array('name' => 'tax','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['Tax']),
                'options' => array('label' => 'Tax')));
        
        $this->add(array('name' => 'discountedPrice','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['PriceSubTotalWithDiscount']),
                'options' => array('label' => 'Discounted Price')));
        
        $this->add(array('name' => 'shipping','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['PriceShipping']),
                'options' => array('label' => 'Shipping')));
        
        $this->add(array('name' => 'total','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['PriceTotal']),
                'options' => array('label' => 'Total')));
        
        
    }
    
    private function _setCardInfo(){
        $ccTypes = array('0'=>'Select CC Type', '1'=>'Visa', '2'=>'Master Card', '3'=>'Discover', '4'=>'American Express');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'cCTypeID',
            'options' => array('label' => 'Order Status','value_options' => $ccTypes),'attributes' => array('class'=>'field-medium','value'=>$this->cart['CCTypeID']),
        ));
        
        $this->add(array('name' => 'cCNumber','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->cart['CCNumber']),
                'options' => array('label' => 'Cc Number')));
        
        $this->add(array('name' => 'cCExpMonth','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  $this->cart['CCExpMonth']),
                'options' => array('label' => 'Cc Exp Month')));
        
        $this->add(array('name' => 'cCExpYear','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  $this->cart['CCExpYear']),
                'options' => array('label' => 'Cc Exp Year')));
        
        $this->add(array('name' => 'cCCode','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  $this->cart['CCCode']),
                'options' => array('label' => 'Cc CVV2')));
        
    }
    
    private function _setFulfilmentInfo()
    {
        $vandors = array('0'=>'Select Vendor', 'Lynd'=>'Lynd', 'Cheng'=>'Cheng');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'vendor',
            'options' => array('label' => 'Vendor','value_options' => $vandors),'attributes' => array('class'=>'field-medium','value'=>$this->cart['Vendor']),
        ));
        
        $options = array('Y'=>'Yes', 'N'=>'No');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'exported',
            'options' => array('label' => 'Exported','value_options' => $options),'attributes' => array('class'=>'field-medium','value'=>$this->cart['Exported']),
        ));

    }


    private function _setSubmit() {
        $this->add(array('name' => 'submit',
            'attributes' => array('type' => 'submit','value' => 'Confirm and Pay order','id' => 'submitbutton','class'=>'button'),
        ));
    }
}
