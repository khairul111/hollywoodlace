<?php

namespace Admin\Form;

use Zend\Form\Form;

class CategoryForm extends Form {

    public $category;
    public function __construct($category) {
        parent::__construct('search');
        $this->category = $category;
        $this->_setFields();
    }
    
    private function _setFields()
    {
        $this->add(array('name' => 'CategoryName','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  $this->category['CategoryName']),
                'options' => array('label' => ' Category Name')));
        $this->add(array('name' => 'SortKey','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  $this->category['SortKey']),
                'options' => array('label' => 'Sort Key')));
        
        $this->add(array('name' => 'HTML1','attributes' => array('type' => 'textarea', 'class'=>'text large','value'=>  $this->category['HTML1'], 'rows'=>10, 'cols'=>80),
                'options' => array('label' => 'Description')));
    }

}
