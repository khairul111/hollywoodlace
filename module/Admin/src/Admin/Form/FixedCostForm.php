<?php

namespace Admin\Form;

use Zend\InputFilter;
use Zend\Form\Form;

class FixedCostForm extends Form {

    public $fixedCost;
    public function __construct($fixedCost = array()) {
        parent::__construct('fixed-cost');
        $this->fixedCost = $fixedCost;
        $this->_setFields();
        $this->addInputFilter();
    }
    
    private function _setFields()
    {
        $years = array();
        for($year=Date("Y");$year<=Date("Y")+3;$year++){
            $years[$year] = $year;
        }
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'FixedCostYear',
            'options' => array('label' => 'Year','value_options' => $years),'attributes' => array('value'=>(array_key_exists('FixedCostYear', $this->fixedCost))?$this->fixedCost['FixedCostYear']:''),));
        
        $months = array();
        for($month=1; $month<=12;$month++){
            $months[$month] = $month;
        }
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'FixedCostMonth',
            'options' => array('label' => 'Month','value_options' => $months),'attributes' => array('value'=>(array_key_exists('FixedCostMonth', $this->fixedCost))?$this->fixedCost['FixedCostMonth']:''),));
        
        
        $this->add(array('name' => 'Employees','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('Employees', $this->fixedCost))?$this->fixedCost['Employees']:'0.00'),
                'options' => array('label' => 'Employees', 'required'=>'required')));
        
        $this->add(array('name' => 'Rent','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=> '1850.00', 'readonly'=>true),
                'options' => array('label' => 'Rent')));
        
        $this->add(array('name' => 'Advertising','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('Advertising', $this->fixedCost))?$this->fixedCost['Advertising']:'0.00'),
                'options' => array('label' => 'Advertising', 'required'=>'required')));
        
        $this->add(array('name' => 'OfficeSupplies','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('OfficeSupplies', $this->fixedCost))?$this->fixedCost['OfficeSupplies']:'0.00'),
                'options' => array('label' => 'Office Supplies', 'required'=>'required')));
        
        $this->add(array('name' => 'PhoneSystem','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('PhoneSystem', $this->fixedCost))?$this->fixedCost['PhoneSystem']:'0.00'),
                'options' => array('label' => 'Phone System', 'required'=>'required')));
        
        $this->add(array('name' => 'TransactionFees','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=> '2.50', 'readonly'=>true),
                'options' => array('label' => 'Transaction Fees', 'required'=>'required')));
        
        $this->add(array('name' => 'Miscellaneous','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('Miscellaneous', $this->fixedCost))?$this->fixedCost['Miscellaneous']:'0.00'),
                'options' => array('label' => 'Miscellaneous', 'required'=>'required')));
        
        $this->add(array('name' => 'Other','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('Other', $this->fixedCost))?$this->fixedCost['Other']:'0.00'),
                'options' => array('label' => 'Other', 'required'=>'required')));
        
        
    }
    
    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();

        // File Input
        $Employees = new InputFilter\Input('Employees');
        $Employees->setRequired(true);      
        $inputFilter->add($Employees);
        
        // File Input
        $Rent = new InputFilter\Input('Rent');
        $Rent->setRequired(true);      
        $inputFilter->add($Rent);
        
        // File Input
        $Advertising = new InputFilter\Input('Advertising');
        $Advertising->setRequired(true);      
        $inputFilter->add($Advertising);
        
        $OfficeSupplies = new InputFilter\Input('OfficeSupplies');
        $OfficeSupplies->setRequired(true);      
        $inputFilter->add($OfficeSupplies);
        
        $PhoneSystem = new InputFilter\Input('PhoneSystem');
        $PhoneSystem->setRequired(true);      
        $inputFilter->add($PhoneSystem);
        
        $TransactionFees = new InputFilter\Input('TransactionFees');
        $TransactionFees->setRequired(true);      
        $inputFilter->add($TransactionFees);
        
        $Miscellaneous = new InputFilter\Input('Miscellaneous');
        $Miscellaneous->setRequired(true);      
        $inputFilter->add($Miscellaneous);
        
        $this->setInputFilter($inputFilter);
    }

}
