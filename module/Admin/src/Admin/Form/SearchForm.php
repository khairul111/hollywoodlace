<?php

namespace Admin\Form;

use Zend\Form\Form;

class SearchForm extends Form {
    private $_startDate, $_endDate;
    public function __construct($date = array()) {
        parent::__construct('search');
        $this->setDate($date);
        $this->_setFields();
    }
    
    public function setDate($date){
        if(count($date) == 0){
            $this->_startDate = date("m/d/Y",strtotime(date("Y-m-d",strtotime(date("Y-m-d")))."-20 days"));
            $this->_endDate = date('m/d/Y');
        }else{
            $this->_startDate = $date['startDate'];
            $this->_endDate = $date['endDate'];
        }
        
    }
    
    private function _setFields()
    { 
        $this->add(array('name' => 'startDate','attributes' => array('type' => 'text', 'class'=>'text short datepicker', 'value'=>  $this->_startDate),
                'options' => array('label' => '')));
        $this->add(array('name' => 'endDate','attributes' => array('type' => 'text', 'class'=>'text short datepicker', 'value'=>  $this->_endDate),
                'options' => array('label' => '')));
        
        $this->add(array('name' => 'firstName','attributes' => array('type' => 'text', 'class'=>'text short'),
                'options' => array('label' => 'First Name')));
        $this->add(array('name' => 'lastName','attributes' => array('type' => 'text', 'class'=>'text short'),
                'options' => array('label' => 'Last Name')));
        
        $this->add(array('name' => 'orderNo','attributes' => array('type' => 'text', 'class'=>'text short'),
                'options' => array('label' => 'Order Number')));
        
        $exportStatur = array('0'=>'All', 'exported'=>'Only Exported', 'nonExported'=>'Only Non-Exported');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'exportStatus',
            'options' => array('label' => 'Exported','value_options' => $exportStatur),'attributes' => array(),
        ));
        
        $vendor = array('All', 'Lynd'=>'Lynd', 'Cheng'=>'Cheng');
        $this->add(array('type' => 'Zend\Form\Element\Select','name' => 'vendor',
            'options' => array('label' => 'Vendor','value_options' => $vendor),'attributes' => array(),
        ));
        
        $this->add(array('type' => 'Zend\Form\Element\Checkbox','name' => 'displayMail','options' => array('label' => 'Display Will Mail In Sample'),));
        
        $this->add(array('type' => 'Zend\Form\Element\Checkbox','name' => 'displayUnsentPaid','options' => array('label' => 'Display Paid Not Sent'),));
        
        $this->add(array('type' => 'Zend\Form\Element\Checkbox','name' => 'displayStock','options' => array('label' => 'Display Stock System'),));
        
        $this->add(array('type' => 'Zend\Form\Element\Checkbox','name' => 'displayCustomSystem','options' => array('label' => 'Display Custom System'),));
        
        $this->add(array('type' => 'Zend\Form\Element\Checkbox','name' => 'displayOnlyProduct','options' => array('label' => 'Display Products Only'),));
        
        $this->add(array('type' => 'Zend\Form\Element\Checkbox','name' => 'suppliesSent','options' => array('label' => 'Supplies Sent'),));
    }

}
