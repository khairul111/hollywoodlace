<?php

namespace Admin\Form;

use Zend\Form\Form;

class ProductForm extends Form {

    public $product;
    public function __construct($product = null) {
        parent::__construct('product');
        $this->product = ($product)?$product:array();
        $this->_setFields();
    }
    
    private function _setFields()
    {
        
        $this->add(array('name' => 'CategoryID','attributes' => array('type' => 'hidden', 'value'=> (array_key_exists('CategoryID', $this->product))?$this->product['CategoryID']:'')));
        
        $this->add(array('name' => 'OldProductID','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=> (array_key_exists('OldProductID', $this->product))?$this->product['OldProductID']:''),
                'options' => array('label' => 'Old Product ID')));
        
        $this->add(array('type' => 'Zend\Form\Element\Checkbox','name' => 'HideOnProductPage',
            'options' => array('label' => "Hide On Product Page",'checked_value' => 'Y', 'unchecked_value' => 'N'),
            'attributes'=>array('value'=>  (array_key_exists('HideOnProductPage', $this->product))?$this->product['HideOnProductPage']:''),
        ));
        
        $this->add(array('name' => 'ProductName','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  (array_key_exists('ProductName', $this->product))?$this->product['ProductName']:''),
                'options' => array('label' => 'Product Name')));
        
        
        $this->add(array('name' => 'VendorProductName','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=>  (array_key_exists('VendorProductName', $this->product))?$this->product['VendorProductName']:''),
                'options' => array('label' => 'Vendor Product Name')));
        
        $this->add(array('name' => 'ProductDescription','attributes' => array('type' => 'textarea', 'class'=>'text large tinymce','value'=> (array_key_exists('ProductDescription', $this->product))?$this->product['ProductDescription']:'', 'rows'=>10, 'cols'=>80),
                'options' => array('label' => 'Description')));
        
        $this->add(array('name' => 'ProductDescription2','attributes' => array('type' => 'textarea', 'class'=>'text large','value'=> (array_key_exists('ProductDescription2', $this->product))?$this->product['ProductDescription2']:'', 'rows'=>10, 'cols'=>80),
                'options' => array('label' => 'Description 2')));
        
        $this->add(array('name' => 'ProductPrice1','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('ProductPrice1', $this->product))?$this->product['ProductPrice1']:''),
                'options' => array('label' => 'Product Price')));
        
        $this->add(array('name' => 'ShippingPrice1','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('ShippingPrice1', $this->product))?$this->product['ShippingPrice1']:''),
                'options' => array('label' => 'Shipping Price')));
        
        $this->add(array('name' => 'ShippingPrice2','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('ShippingPrice2', $this->product))?$this->product['ShippingPrice2']:''),
                'options' => array('label' => 'Shipping Price 2')));
        
        $this->add(array('name' => 'HairCutPrice','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('HairCutPrice', $this->product))?$this->product['HairCutPrice']:''),
                'options' => array('label' => 'Hair Cut Price')));
        
        $this->add(array('name' => 'FullCapPrice','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('FullCapPrice', $this->product))?$this->product['FullCapPrice']:''),
                'options' => array('label' => 'Full Cap Price')));
        
        $this->add(array('name' => 'ChengFullCapPrice','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('ChengFullCapPrice', $this->product))?$this->product['ChengFullCapPrice']:''),
                'options' => array('label' => 'Cheng Full Cap Price')));
        
        $this->add(array('name' => 'LyndFullCapPrice','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('LyndFullCapPrice', $this->product))?$this->product['LyndFullCapPrice']:''),
                'options' => array('label' => 'Lynd Full Cap Price')));
        
        $this->add(array('name' => 'ProductPriceCheng','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('ProductPriceCheng', $this->product))?$this->product['ProductPriceCheng']:''),
                'options' => array('label' => 'Cheng Product Price')));
        
        $this->add(array('name' => 'ProductPriceLynd','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('ProductPriceLynd', $this->product))?$this->product['ProductPriceLynd']:''),
                'options' => array('label' => 'Cheng Lynd Product Price')));
        
        $this->add(array('name' => 'RatingImageUrl','attributes' => array('type' => 'text', 'class'=>'text long', 'value'=>  (array_key_exists('RatingImageUrl', $this->product))?$this->product['RatingImageUrl']:''),
                'options' => array('label' => 'Rating Image URL')));
        
        $this->add(array('name' => 'FeaturesUrl','attributes' => array('type' => 'text', 'class'=>'text long', 'value'=>  (array_key_exists('FeaturesUrl', $this->product))?$this->product['FeaturesUrl']:''),
                'options' => array('label' => 'Features Url')));
        
        $this->add(array('name' => 'SortKey','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('SortKey', $this->product))?$this->product['SortKey']:''),
                'options' => array('label' => 'SortKey')));
        
    }

}
