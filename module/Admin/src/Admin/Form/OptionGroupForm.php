<?php

namespace Admin\Form;

use Zend\Form\Form;

class OptionGroupForm extends Form {

    public $product;
    public function __construct($product = null) {
        parent::__construct('optionGroup');
        $this->optionGroup = ($product)?$product:array();
        $this->_setFields();
    }
    
    private function _setFields()
    {
        
        $this->add(array('name' => 'ProductID','attributes' => array('type' => 'hidden', 'value'=> (array_key_exists('ProductID', $this->optionGroup))?$this->optionGroup['ProductID']:'')));
        
        $this->add(array('name' => 'OptionGroupName','attributes' => array('type' => 'text', 'class'=>'text medium', 'value'=> (array_key_exists('OptionGroupName', $this->optionGroup))?$this->optionGroup['OptionGroupName']:''),
                'options' => array('label' => 'Option Name')));
                
        $this->add(array('name' => 'OptionGroupDescription','attributes' => array('type' => 'textarea', 'class'=>'text large tinymce','value'=> (array_key_exists('OptionGroupDescription', $this->optionGroup))?$this->optionGroup['OptionGroupDescription']:'', 'rows'=>10, 'cols'=>80),
                'options' => array('label' => 'Description')));
        
        $this->add(array('name' => 'PopupTitle','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('PopupTitle', $this->optionGroup))?$this->optionGroup['PopupTitle']:''),
                'options' => array('label' => 'Popup Titile')));
        
        $this->add(array('name' => 'SortKey','attributes' => array('type' => 'text', 'class'=>'text short', 'value'=>  (array_key_exists('SortKey', $this->optionGroup))?$this->optionGroup['SortKey']:''),
                'options' => array('label' => 'SortKey')));
        
    }

}
