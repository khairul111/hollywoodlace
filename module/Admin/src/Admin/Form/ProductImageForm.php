<?php

namespace Admin\Form;

use Zend\InputFilter;
use Zend\Form\Element;
use Zend\Form\Form;

class ProductImageForm extends Form {

    public  $productId;
    public function __construct($productId) {
        parent::__construct('product-image');
        $this->productId = $productId;
        $this->addElements();
        $this->addInputFilter();
    }
    
    public function addElements()
    {
        // File Input
        $file = new Element\File('image-file');
        $file->setLabel('Photo')
             ->setAttribute('id', 'image-file');
        $this->add($file);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();

        // File Input
        $fileInput = new InputFilter\FileInput('image-file');
        $fileInput->setRequired(true);
        $fileInput->getFilterChain()->attachByName(
            'filerenameupload',
            array(
                'target'    => './public/images/product/'.substr('00000000'.$this->productId,-8).'.jpg',
            )
        );
        $inputFilter->add($fileInput);

        $this->setInputFilter($inputFilter);
    }
}
