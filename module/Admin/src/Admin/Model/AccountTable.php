<?php
namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class AccountTable extends AbstractTableGateway
{
    protected $table ='WebStoreAccounts';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('AccountID'=>$id));
        return $resultSet->current();
    }
    
    public function getByUserName($userName)
    {
        $resultSet = $this->select(array('UsrLogin'=>$userName));
        return $resultSet->current();
    }
    
    public function fetchAll()
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreAccounts');
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();        
    }
    
    public function save(array $user, $id=0){
        $user['LastUpdate'] = date('Y-m-d h:i:s');
        if ($id == 0) {
            $user['IPAddress'] = $_SERVER['REMOTE_ADDR']; 
            $this->insert($user);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->update($user, array('AccountID' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }
    
    public function deleteItem($id){
        return $this->delete('AccountID ='. $id);
    }
    
    public function prepareMenu($userName, $sm){
        $menu = array();
        $user = $this->getByUserName($userName);
        $aclTable = $sm->get('Admin\Model\AclResourceTable');
        if($userName == 'admin'){
            $accesses = $aclTable->fetchAll();
        }else{
            $accesses = $aclTable->getAllByIds($user['AccessLevel']);
        }
        
        foreach($accesses as $key=>$access){
            $pageName = explode(':',$access['PageName']);
            $menu[$pageName[0]][$key]['name'] = $pageName[1];
            $menu[$pageName[0]][$key]['url'] = $access['FileName'];
        }
        
        return $menu;
    }
}
