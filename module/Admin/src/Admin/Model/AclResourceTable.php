<?php
namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class AclResourceTable extends AbstractTableGateway
{
    protected $table ='WebStoreAdminPages';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('AccountID'=>$id));
        return $resultSet->current();
    }
    
    public function getAllByIds($ids){
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('WebStoreAdminPages')
                ->where("PageID in ($ids)");
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
    
    
    public function save(array $discountCode, $id=0){
        $discountCode['LastUpdate'] = date('Y-m-d h:i:s');
        if ($id == 0) {
            $this->insert($discountCode);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->update($discountCode, array('DiscountCodeID' => $id));
            } else {
                throw new \Exception('Category  id does not exist');
            }
        }
    }
    
    public function deleteItem($id){
        return $this->delete('DiscountCodeID ='. $id);
    }
}
