<?php
namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class FixedCostTable extends AbstractTableGateway
{
    protected $table ='FixedCost';
   
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    
    public function getById($id)
    {
        $resultSet = $this->select(array('FixedCostID'=>$id));
        return $resultSet->current();
    }
    
    public function getByMontAndYear($month, $year)
    {
        $resultSet = $this->select(array('FixedCostMonth'=>$month,'FixedCostYear'=>$year));
        return $resultSet->current();
    }
    public function fetchAll()
    {
        $adapter = $this->adapter;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('FixedCost')
                ->order(array('FixedCostYear DESC', 'FixedCostMonth DESC', 'FixedCostID DESC'));
        
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();        
    }
    
    public function save(array $fixedCost, $id=0){
        $fixedCost['LastUpdate'] = date('Y-m-d h:i:s');
        if ($id == 0) {
            $fixedCost['IPAddress'] = $_SERVER['REMOTE_ADDR']; 
            $this->insert($fixedCost);
            return $this->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->update($fixedCost, array('FixedCostID' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }
    
    public function deleteItem($id){
        return $this->delete('FixedCostID ='. $id);
    }
    
    public function getFixedCostOfMonth($startDate,$endDate){
        $numberDays=(strtotime($endDate)-strtotime($startDate))/(60*60*24)+1;
        $fixedCost=0;
        $queriedTime = array('month'=>'', 'year'=>'');
        $totalFixedCost = 0;
        $fixedCostPerDay = 0;
	for($i=0;$i<$numberDays;$i++){
            $fixedCostYear=date("Y",strtotime(date("Y-m-d",strtotime($startDate))."+$i days"));
            $fixedCostMonth=date("m",strtotime(date("Y-m-d",strtotime($endDate))."+$i days"));
            if($queriedTime['month'] != $fixedCostMonth && $queriedTime['year'] = $fixedCostYear){
                $fixedCost = $this->getByMontAndYear($fixedCostMonth, $fixedCostYear);
                $fixedCostPerDay = ($fixedCost)?$fixedCost['FixedCostPerDay']:0;
                $queriedTime = array('month'=>$fixedCostMonth, 'year'=>$fixedCostYear);
            }
            
            $totalFixedCost = $totalFixedCost + $fixedCostPerDay;
	  
        }
        
        return $totalFixedCost;
    }
}
