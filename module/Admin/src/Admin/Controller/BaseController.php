<?php

namespace Admin\Controller;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;

class BaseController extends AbstractActionController
{
    public $_data = array();
    public $_view;
    protected $authservice;
    public $sm;


    public function setEventManager(EventManagerInterface $events) {
        parent::setEventManager($events);

        $controller = $this;
        $events->attach('dispatch', function ($e) use ($controller) {
                    call_user_func(array($controller,'authenticate'));
                }, 100); 
    }
    
    public function authenticate()
    { 
        $this->authservice = new AuthenticationService();
        $this->data['test']= 'test';
        if(!$this->authservice->hasIdentity()){
            return $this->redirect()->toRoute('admin-auth', array('action'=>'login'));
        }
        $layout = $this->layout();
        $layout->identity = $this->authservice->getIdentity();
        $sm = $this->getServiceLocator();
        $accountTable = $sm->get('Admin\Model\AccountTable');
        $layout->menu = $accountTable->prepareMenu($layout->identity, $sm);
    }
    
    public function activePaginator(array $result)
    {
        $paginator = new Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($result));
        $paginator->setCurrentPageNumber($this->params()->fromRoute('id'));
        $paginator->setItemCountPerPage(20);
        $this->_data['list'] = $paginator;
    }
    
    private function _prepareMainMenu()
    {
        $menu = array();   
        $this->_data['mainMenu'] = $menu;
    }
    
    
        
}
