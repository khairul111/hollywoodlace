<?php

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;

class AccountController extends BaseController
{

    public function userAction()
    {
        $this->layout()->pageTitle = "User List";
        $sm = $this->getServiceLocator();
        $accountTable = $sm->get('Admin\Model\AccountTable');
        $this->activePaginator($accountTable->fetchAll());
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function addAction()
    {
        $this->layout()->pageTitle = "Add New User";
        $sm = $this->getServiceLocator();
        $userTable = $sm->get('Admin\Model\AccountTable');
        $userFrom = new \Admin\Form\UserForm(array(), $sm);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $userFrom->setData($request->getPost());
            if ($userFrom->isValid()) {
                $data = $userFrom->getData();
                $data['AccessLevel'] = implode(',', $data['resources']);
                unset ($data['resources']);
                $userTable->save($data);
                return $this->redirect()->toRoute('admin-account', array('action'=>'user'));
            }
        }
        
        $view = new ViewModel(array('form'=>$userFrom));
        $view->setTemplate('admin/account/form');
        return $view;
    }
    
    public function editAction()
    {
        $this->layout()->pageTitle = "Edit User";
        $userId = $this->params()->fromRoute('id', 1);
        $sm = $this->getServiceLocator();
        $userTable = $sm->get('Admin\Model\AccountTable');
        $user = $userTable->getById($userId);
        
        $userFrom = new \Admin\Form\UserForm($user, $sm);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $userFrom->setData($request->getPost());
            if ($userFrom->isValid()) {
                $data = $userFrom->getData();
                $data['AccessLevel'] = implode(',', $data['resources']);
                unset ($data['resources']);
                $userTable->save($data, $userId);
                return $this->redirect()->toRoute('admin-account', array('action'=>'user'));
            }
        }
        
        $view = new ViewModel(array('form'=>$userFrom));
        $view->setTemplate('admin/account/form');
        return $view;
    }
    
    public function deleteAction(){
        $userId = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $accountTable = $sm->get('Admin\Model\AccountTable');
        $accountTable->deleteItem($userId);
        return $this->redirect()->toRoute('admin-account', array('action'=>'user'));
    }
    
    public function downloadAction()
{
    // magically create $content as a string containing CSV data

    $response = $this->getResponse();
    $response->getHeaders();
    $headers->addHeaderLine('Content-Type', 'text/csv');
    $headers->addHeaderLine('Content-Disposition', "attachment; filename=\"my_filen.csv\"");
    $headers->addHeaderLine('Accept-Ranges', 'bytes');
    $headers->addHeaderLine('Content-Length', strlen($content));

    $response->setContent($content);
    return $response;
}
}
