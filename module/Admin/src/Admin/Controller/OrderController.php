<?php

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class OrderController extends BaseController
{

    public function indexAction()
    { 
        $view = new ViewModel($this->_data);
        $this->layout()->pageTitle = "Welcome to Adminstrator Control Panel";
        return $view;
    }
    
    public function listAction()
    {
        $this->layout()->pageTitle = "Oder List";
        $this->_data['orederBy'] = $this->params()->fromRoute('orderby', 'CartID-desc');
        $orderby = explode('-', $this->_data['orederBy']);
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $this->activePaginator($cartTable->fetchAll($orderby[0].' '.$orderby[1]));
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id', 0);
        $this->layout()->pageTitle = "Order #".$id;
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $cart = $cartTable->getById($id);
        $orderForm = new \Admin\Form\OrderForm($cart);
        $billingForm = new \Application\Form\BillingForm($sm, array('cart'=>$cart));
        $request = $this->getRequest();
        if ($request->isPost()) {
            $orderForm->setData($request->getPost());
            $billingForm->setData($request->getPost());

            if ($orderForm->isValid()) {
                $cartData = $this->_prepareOrderData($request->getPost());
                $cartTable->save($cartData, $id);
                return $this->redirect()->toRoute('admin-order', array('action'=>'report'));
            }
        }

        return array('form'=>$orderForm, 'billingForm'=>$billingForm, 'id'=>$id, 'added'=>$cart['Added'], 
            'updated'=>($cart['Updated']=='0000-00-00 00:00:00')?$cart['Updated']:$cart['Added']);     
    }
    
    public function reportAction(){
        $this->layout()->pageTitle = "Order Report";
        $sm = $this->getServiceLocator();
        $searchForm = new \Admin\Form\SearchForm();
        $cartTable = $sm->get('Application\Model\CartTable');
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $startDate = date("m/d/Y",strtotime(date("Y-m-d",strtotime(date("Y-m-d")))."-20 days"));
        $endDate = date('m/d/Y');
        $data = $cartTable->search(array('searchDate'=>'yes','startDate'=>$startDate, 'endDate'=>$endDate), false);
        foreach ($data as $key => $cart) {
            $data[$key]['accessoriesCount'] = $productCartTable->getAccessoriesCount($cart['CartID']);
            $data[$key]['notAccessoriesCount'] = $productCartTable->getNonAccessoriesCount($cart['CartID']);
            $data[$key]['prices'] = $productCartTable->getAllCartPrice($cart['CartID'], $sm, $data[$key]);
        }
        $view = new ViewModel(array('data'=>$data, 'form'=>$searchForm, 'searchDate' => 'yes'));
        $view->setTemplate('admin/order/report');
        return $view;
    }
    
    public function reorderAction(){
        $id = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $cart = $cartTable->reorder($id, $sm);
        return $this->redirect()->toRoute('admin-order', array('action'=>'report'));
    }
    
    public function searchAction(){
        $data = array();
        $searchForm = new \Admin\Form\SearchForm();
        $searchDate = 'yes';
        $request = $this->getRequest();
        if ($request->isPost()) {
            $searchForm->setData($request->getPost());
            if ($searchForm->isValid()) {
                $sm = $this->getServiceLocator();
                $cartTable = $sm->get('Application\Model\CartTable');
                $productCartTable = $sm->get('Application\Model\ProductCartTable');
                $formData = $request->getPost()->toArray();
                $searchDate = $formData['searchDate'];
                $data = $cartTable->search($request->getPost()->toArray());
                
                foreach ($data as $key => $cart) {
                    $data[$key]['accessoriesCount'] = $productCartTable->getAccessoriesCount($cart['CartID']);
                    $data[$key]['notAccessoriesCount'] = $productCartTable->getNonAccessoriesCount($cart['CartID']);
                    $data[$key]['prices'] = $productCartTable->getAllCartPrice($cart['CartID'], $sm, $data[$key]);
                }
            }            
        }
        $view = new ViewModel(array('data'=>$data, 'form'=>$searchForm, 'searchDate' => 'yes'));
        $view->setTemplate('admin/order/report');
        return $view;
    }
    
    public function userReportAction(){
        $cartId = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $searchForm = new \Admin\Form\SearchForm();
        $cartTable = $sm->get('Application\Model\CartTable');
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $cart = $cartTable->getById($cartId);
        $data = $cartTable->getActiveByBillingEmail($cart['BillingEmail']);
        foreach ($data as $key => $cart) {
            $data[$key]['accessoriesCount'] = $productCartTable->getAccessoriesCount($cart['CartID']);
            $data[$key]['notAccessoriesCount'] = $productCartTable->getNonAccessoriesCount($cart['CartID']);
            $data[$key]['prices'] = $productCartTable->getAllCartPrice($cart['CartID'], $sm, $data[$key]);
        }
        $view = new ViewModel(array('data'=>$data, 'form'=>$searchForm, 'searchDate' => 'yes'));
        $view->setTemplate('admin/order/report');
        return $view;
    }
    
    public function deleteAction(){
        $id = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $cartTable->deleteItem($id);
        
            
        return $this->redirect()->toRoute('admin-order', array('action'=>'list'));

    }
    
    public function multipleDeleteAction(){
        $result = new ViewModel();
        $result->setTerminal(true);
        $cartIds = $this->getRequest()->getPost('ids');
        $cartTable = $this->getServiceLocator()->get('Application\Model\CartTable');
        foreach($cartIds as $cartId){
            $cartTable->deleteItem($cartId);
        }
        exit;

    }
    
    public function updateCartAction(){
        $result = new ViewModel();
        $result->setTerminal(true);
        $cartId = $this->params()->fromRoute('id', 0);
        $data = $this->getRequest()->getPost()->toArray();
        $cartTable = $this->getServiceLocator()->get('Application\Model\CartTable');
        $cartTable->save($data,$cartId);
        exit;
        
        
    }
    
    public function vendorMessageAction(){
        $result = new ViewModel();
        $result->setTerminal(true);
        $cartId = $this->params()->fromRoute('id', 0);
        $cartTable = $this->getServiceLocator()->get('Application\Model\CartTable');
        $cart = $cartTable->getById($cartId);
        $result->setVariables(array('cart'=>$cart));
        return $result;
    }
    
    public function buyerEmailAction(){ 
        $result = new ViewModel();
        $result->setTerminal(true);
        $cartId = $this->params()->fromRoute('id', 0);
        $cartTable = $this->getServiceLocator()->get('Application\Model\CartTable');
        $cart = $cartTable->getById($cartId);
        $result->setVariables(array('cart'=>$cart));
        return $result;
    }
    
    public function sentBuyerEmailAction(){
        $data = $this->getRequest()->getPost();
        $mail = new \Zend\Mail\Message();
        $mail->setBody($data['message']); // will generate our code html from template.phtml  
        $mail->setFrom(NotificationFrom);
        $mail->addTo($data['email']);
        $mail->setSubject($data['subject']);
        
        $transporst = new \Zend\Mail\Transport\Sendmail();
        $transporst->send($mail);
        exit;
    }
    
    public function vendorEmailAction(){ 
        $result = new ViewModel();
        $result->setTerminal(true);
        $sm = $this->getServiceLocator();
        $cartId = $this->params()->fromRoute('id', 0);
        $cartTable = $sm->get('Application\Model\CartTable');
        $cart = $cartTable->getById($cartId);
        $summeryTextPrefix = "The following is a new order from Hollywoodlace. \n\r";
        $summeryTextPrefix .= 'Order ID: '.$cartId."\n\r";
        $summeryTextPrefix .= 'Order date: '.date("F d, Y",strtotime($cart['Added'])).'\n\r';
        $summeryTextPrefix .= 'Customer Name : '.$cart['BillingFirstName'].' '.$cart['BillingLastName']."\n\r";
        $summeryTextPrefix .= "Order Summery \n\r";
        $summeryText = $summeryTextPrefix.$cartTable->getSummeryText($cartId, $sm);
        $result->setVariables(array('cart'=>$cart, 'summeryText'=>$summeryText));
        return $result;
    }
    
    public function sentVendorEmailAction(){
        $data = $this->getRequest()->getPost();
        $cartTable = $this->getServiceLocator()->get('Application\Model\CartTable');
        
        $subject="New Order ".$data['id'].' '.$data['priority'];
        $vendor = '';
        $email = 'alison@sinowigs.com';
        if($data['sentToVendor'] == 'SunColorHair.com'){
            $vendor = 'Lynd';
            $email = 'sdqdhair@126.com';
        }elseif($data['sentToVendor'] == 'HaiChuanHair.com'){
            $vendor = "Cheng";
            $email = 'chengw@haichuanhair.com, cnhaichuanhair@gmail.com';
        }
        $updatedData = array('Vendor'=>$vendor, 'VendorPriority'=>$data['priority'], 'VendorMessageDate'=>date("Y-m-d H:i:s"), 
            'SentToVendorName'=>$data['sentToVendor'], 'SentToVendorEmail'=> $email, 'SubjectToVendor'=>$subject, 'MessageToVendor'=>$data['message']);
        
        $cartTable->save($updatedData, $data['id']);
        
        $mail = new \Zend\Mail\Message();
        $mail->setBody($data['message']); // will generate our code html from template.phtml  
        $mail->setFrom(NotificationFrom);
        $mail->addTo($email);
        $mail->setSubject($subject);
        
        $transporst = new \Zend\Mail\Transport\Sendmail();
//        $transporst->send($mail);
        
        exit;
    }
    
    private function _prepareOrderData($data) {
        $cartData = array('Added' => $data['orderDate'], 'Updated' => date("Y-m-d H:i:s"), 'OrderNotes' => $data['orderNotes'], 'DiscountCode' => $data['discountCode'],
            'ShippingMethod' => $data['shippingMethod'], 'OrderSummary' => $data['summary'], 'CartStatusID' => $data['status'], 'OrderNote' => $data['note'],
            'EstimatedDelivery' => $data['deliveryDate'], 'OrderTXT2' => $data['details'], 'Flags' => $data['flag'], 'Reorder' => $data['reorder'], 'StockSystem' => $data['stockSystem'],
            'PriceSubTotal' => $data['subTotal'], 'Tax' => $data['tax'], 'PriceSubTotalWithDiscount' => $data['discountedPrice'], 'PriceShipping' => $data['shipping'],
            'PriceTotal' => $data['total'], 'FirstName' => $data['firstName'], 'LastName' => $data['lastName'], 'BillingFirstName' => $data['billingFirstName'],
            'BillingLastName' => $data['billingLastName'], 'BillingCountryID' => $data['billingCountry'], 'BillingStateName' => $data['billingStateName'],
            'BillingStateID' => $data['billingState'], 'BillingAddress' => $data['billingAddress'], 'BillingCity' => $data['billingCity'], 'BillingZipCode' => $data['billingZip'],
            'BillingPhone' => $data['billingPhone'], 'BillingEmail' => $data['billingEmail'], 'CCTypeID' => $data['cCTypeID'], 'CCNumber' => $data['cCNumber'],
            'CCExpMonth' => $data['cCExpMonth'], 'CCExpYear' => $data['cCExpYear'], 'CCCode' => $data['cCCode'], 'ShippingFirstName' => $data['shippingFirstName'],
            'ShippingLastName' => $data['shippingLastName'], 'ShippingCountryID' => $data['shippingCountry'], 'ShippingStateID' => $data['shippingState'],
            'ShippingStateName' => $data['shippingStateName'], 'ShippingAddress' => $data['shippingAddress'], 'ShippingCity' => $data['shippingCity'],
            'ShippingZipCode' => $data['shippingZip'], 'ShippingPhone' => $data['shippingPhone'], 'ShippingEmail' => $data['shippingEmail'],
            'Referral' => $data['referral'], 'Vendor' => $data['vendor'], 'Exported' => $data['exported']);
        
        return $cartData;
    }
}
