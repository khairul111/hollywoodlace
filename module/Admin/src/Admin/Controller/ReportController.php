<?php

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\Http\Headers;
use Zend\View\Model\ViewModel;

class ReportController extends BaseController
{

    public function floridaAction()
    {
        $this->layout()->pageTitle = "Florida Report";
        $date['startDate'] = date("m/d/Y",strtotime("-1 Months"));
        $date['endDate'] = date("m/d/Y");
        $request = $this->getRequest();
        if($request->isPost()){
            $date['startDate'] = $request->getPost('startDate');
            $date['endDate'] = $request->getPost('endDate');
        }
        
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
        $startDate = date('Y-m-d', strtotime($date['startDate'])).' 00:00:00';
        $endDate = date('Y-m-d', strtotime($date['endDate'])).' 00:00:00';
        
        $carts = $cartTable->getByStateIdBetweenDate(8,$startDate, $endDate);
        foreach ($carts as $key => $cart) {
            $carts[$key]['accessoriesCount'] = $productCartTable->getAccessoriesCount($cart['CartID']);
            $carts[$key]['notAccessoriesCount'] = $productCartTable->getNonAccessoriesCount($cart['CartID']);
            $carts[$key]['prices'] = $productCartTable->getAllCartPrice($cart['CartID'], $sm, $carts[$key]);
        }
        $this->_data['date'] = $date;
        $this->_data['carts'] = $carts;
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function fixedCostAction()
    {
        $this->layout()->pageTitle = "Fixed Coast For Financial Report";
        $sm = $this->getServiceLocator();
        $fixedCoastTable = $sm->get('Admin\Model\FixedCostTable');
        $this->activePaginator($fixedCoastTable->fetchAll());
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function financialAction()
    {
        $this->layout()->pageTitle = "Financial Reports";
        $this->_data['carts'] = array();
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $fixedCoastTable = $sm->get('Admin\Model\FixedCostTable');
        $formData = array('startDate'=>date('m/d/Y'), 'endDate'=>date('m/d/Y'));
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = array('startDate'=>$request->getPost('startDate'), 'endDate'=>$request->getPost('endDate'));
            $this->_data['carts'] = $cartTable->prepareFinancialReport($request->getPost()->toArray(), $sm);
            $daysNumber = (strtotime($formData['endDate'])-strtotime($formData['startDate']))/(60*60*24)+1;
            $this->_data['fixedCost'] = $daysNumber*1600;//$fixedCoastTable->getFixedCostOfMonth($request->getPost('startDate'),$request->getPost('endDate'));
        }
        $this->_data['formData'] = $formData;
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function newFixedCostAction()
    {
        $this->layout()->pageTitle = "New Fixed Cost";
        $sm = $this->getServiceLocator();
        $fixedCostTable = $sm->get('Admin\Model\FixedCostTable');
        $fixedCostFrom = new \Admin\Form\FixedCostForm();
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fixedCostFrom->setData($request->getPost());
            if ($fixedCostFrom->isValid()) {
                $fixedCostTable->save($fixedCostFrom->getData(), $fixedCostId);
                return $this->redirect()->toRoute('admin-report', array('action'=>'fixed-cost'));
            }
        }
        $view = new ViewModel(array('form'=>$fixedCostFrom));
        $view->setTemplate('admin/report/fixed-cost-form');
        return $view;
    }
    
    public function editFixedCostAction()
    {
        $this->layout()->pageTitle = "Edit Fixed Cost";
        $fixedCostId = $this->params()->fromRoute('id', 1);
        $sm = $this->getServiceLocator();
        $fixedCostTable = $sm->get('Admin\Model\FixedCostTable');
        $fixedCost = $fixedCostTable->getById($fixedCostId);
        $fixedCostFrom = new \Admin\Form\FixedCostForm($fixedCost);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fixedCostFrom->setData($request->getPost());
            if ($fixedCostFrom->isValid()) {
                $fixedCostTable->save($fixedCostFrom->getData(), $fixedCostId);
                return $this->redirect()->toRoute('admin-report', array('action'=>'fixed-cost'));
            }
        }
        $view = new ViewModel(array('form'=>$fixedCostFrom));
        $view->setTemplate('admin/report/fixed-cost-form');
        return $view;
    }
    
    public function deleteFixedCostAction(){
        $fixedCostId = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $fixedCoastTable = $sm->get('Admin\Model\FixedCostTable');
        $fixedCoastTable->deleteItem($fixedCostId);
        return $this->redirect()->toRoute('admin-report', array('action'=>'fixed-cost'));
    }
    
    public function vendorAction(){
        $this->layout()->pageTitle = "Vendor Report";
        $data = array();
        $startDate = date("m/d/Y",strtotime(date("Y-m-d",strtotime(date("Y-m-d")))."-1 month"));
        $endDate = date('m/d/Y');
        $searchForm = new \Admin\Form\SearchForm(array('startDate'=>$startDate, 'endDate'=>$endDate));
        $searchDate = 'yes';
        $request = $this->getRequest();
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
                        
        if ($request->isPost()) {
            $searchForm->setData($request->getPost());
            $productCartTable = $sm->get('Application\Model\ProductCartTable');
            $formData = $request->getPost()->toArray();
            $searchDate = $formData['searchDate'];
            $data = $cartTable->search($request->getPost()->toArray(), false);
        }  else {
            $data = $cartTable->search(array('searchDate'=>'yes','startDate'=>$startDate, 'endDate'=>$endDate), false);
        }
        $reorder = 0;
        $totalPrice = 0;
        foreach($data as $key=>$cart){
            $data[$key]['products'] = $productCartTable->getByCartId($cart['CartID']);
            $data[$key]['prevInvoice'] = $productCartTable->getReorderInfo($cart['CartID'], $cart['BillingEmail'], $data[$key]['products']);
            $reorder = ($data[$key]['prevInvoice'])? $reorder +1:$reorder;
            $totalPrice += $cart['PriceTotal'];
        }
        $summery['reorderCount'] = $reorder;
        $summery['totalPrice'] = $totalPrice;
        $summery['totalRecord'] = count($data);
        $view = new ViewModel(array('data'=>$data, 'form'=>$searchForm, 'summery'=>$summery, 'searchDate' => 'yes'));
        return $view;
    }
    
    public function csvVendorAction(){
        $data = array();
        $response = $this->getEvent()->getResponse();
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=Leads.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        
        $searchForm = new \Admin\Form\SearchForm();
        $searchDate = 'yes';
        $request = $this->getRequest();
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $productCartTable = $sm->get('Application\Model\ProductCartTable');
                        
        if ($request->isPost()) {
            $searchForm->setData($request->getPost());
            $productCartTable = $sm->get('Application\Model\ProductCartTable');
            $formData = $request->getPost()->toArray();
            $searchDate = $formData['searchDate'];
            $data = $cartTable->searchForVendor($request->getPost()->toArray());
        }
        $reorder = 0;
        $totalPrice = 0;
        foreach($data as $key=>$cart){
            $phone = $cart['BillingPhoneCode'].'('.$cart['BillingPhone1'].')'.$cart['BillingPhone2'].'-'.$cart['BillingPhone3'].' '.$cart['BillingPhoneExt'];
            $name = $cart['BillingFirstName'].' '.$cart['BillingLastName'];
            echo $name.';'.$cart['Added'].';'.$cart['OrderNotes'].';'.$cart['DiscountCode'].';'.$cart['ShippingMethod'].';'.$cart['OrderSummary'].';'.$cart['OrderNote'].';'.str_replace("0000-00-00","",$cart['EstimatedDelivery']).';'
                    .$cart['OrderTXT2'].';'.$cart['Reorder'].';'.$cart['PriceSubTotal'].';'.$cart['Tax'].';'.$phone.';'.$cart['BillingEmail'].';'.$cart['Vendor']."\n";
        }
        
        return $response;
    }
    
    public function leadAction(){
        $this->layout()->pageTitle = "Leads Report";
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $carts = $cartTable->getAllLeads();
        return array('carts'=>$carts);    
    }
    
    public function exportCustomerAction(){
        $this->layout()->pageTitle = "Export Customer";
        $this->_data['startDate'] = date("m/d/Y",strtotime(date("Y-m-d",strtotime(date("Y-m-d")))."-20 days"));
        $this->_data['endDate'] = date('m/d/Y');
        
        return $this->_data;
    }
    
    public function executeExportCustomerAction(){
        $response = $this->getEvent()->getResponse();
        $data = $this->getRequest()->getPost()->toArray();
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=Customers.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $startDate = date('Y-m-d', strtotime($data['startDate'])).' 00:00:00';
        $endDate = date('Y-m-d', strtotime($data['endDate'])).' 23:59:59';
        $customers = $cartTable->getAllStateCustomerBetweenDate($startDate, $endDate);
        echo "Name; State Code; Address; City; Zip; Phone; Email; \n";
        foreach($customers as $key=>$customer){
            $phone = $customer['BillingPhoneCode'].'('.$customer['BillingPhone1'].')'.$customer['BillingPhone2'].'-'.$customer['BillingPhone3'].' '.$customer['BillingPhoneExt'];
            $name = $customer['BillingFirstName'].' '.$customer['BillingLastName'];
            echo $name.';'.$customer['StateCode'].';'.$customer['BillingAddress'].';'.$customer['BillingCity'].';'.$customer['BillingZipCode'].';'.$phone.';'.$customer['BillingEmail']."\n";
            
        }
        return $response;
    }
    
    public function exportLeadAction(){
        $this->layout()->pageTitle = "Export Lead";
        $this->_data['startDate'] = date("m/d/Y",strtotime(date("Y-m-d",strtotime(date("Y-m-d")))."-20 days"));
        $this->_data['endDate'] = date('m/d/Y');
            
        return $this->_data;
    }
    
    public function executeExportLeadAction(){
        $response = $this->getEvent()->getResponse();
        $data = $this->getRequest()->getPost()->toArray();
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=Leads.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\CartTable');
        $startDate = date('Y-m-d', strtotime($data['startDate'])).' 00:00:00';
        $endDate = date('Y-m-d', strtotime($data['endDate'])).' 23:59:59';
        $customers = $cartTable->getAllStateCustomerBetweenDate($startDate, $endDate);
        echo "Name; State Code; Address; City; Zip; Phone; Email; \n";
        foreach($customers as $key=>$customer){
            $name = $customer['BillingFirstName'].' '.$customer['BillingLastName'];
            echo $customer['BillingEmail'].';'.$name.';'.$customer['Added']."\n";
            
        }
        return $response;
    }
}

