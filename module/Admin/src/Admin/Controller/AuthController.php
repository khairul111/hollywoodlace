<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use Admin\Form\LoginForm;

class AuthController extends AbstractActionController
{
    protected $authservice;
    protected $form;
    
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }   
        
        return $this->authservice;
    }

    public function loginAction()
    {
        if ($this->getAuthService()->hasIdentity()){
            return $this->redirect()->toRoute('admin-order');
        }
        $this->layout()->identity = false;
        $loginForm = new LoginForm();
        return array('form'      => $loginForm,
            'messages'  => $this->flashmessenger()->getMessages());
    }
    
    public function authenticateAction()
    {
        $form       = new LoginForm();
        $redirect = 'login';
        
        $request = $this->getRequest();
        if ($request->isPost()){
            $form->setData($request->getPost());
            if ($form->isValid()){
                $this->getAuthService()->getAdapter()
                                       ->setIdentity($request->getPost('username'))
                                       ->setCredential($request->getPost('password'));
                                       
                $result = $this->getAuthService()->authenticate();
                foreach($result->getMessages() as $message)
                {
                    //save message temporary into flashmessenger
                    $this->flashmessenger()->addMessage($message);
                }
                
                if ($result->isValid()) {
                    $redirect = 'success';
                    //check if it has rememberMe :
                    
                    $this->getAuthService()->getStorage()->write($request->getPost('username'));
                }
            }
        }
        
        return $this->redirect()->toRoute('admin-auth', array('action'=>'login'));
    }
    
    public function logoutAction()
    {
        $this->getAuthService()->clearIdentity();
        
        $this->flashmessenger()->addMessage("You've been logged out");
        return $this->redirect()->toRoute('admin-auth', array('action'=>'login'));
    }
    
    public function getForm()
    {
        if (! $this->form) {
            $login      = new LoginForm();
            $builder    = new AnnotationBuilder();
            $this->form = $builder->createForm($login);
        }        
        return $this->form;
    }
    
}
