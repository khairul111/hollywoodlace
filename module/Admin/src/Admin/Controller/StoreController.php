<?php

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;

class StoreController extends BaseController
{

    public function categoryAction()
    {
        $this->layout()->pageTitle = "Category List";
        $this->_data['orederBy'] = $this->params()->fromRoute('orderby', 'CategoryID-desc');
        $orderby = explode('-', $this->_data['orederBy']);
        $sm = $this->getServiceLocator();
        $categoryTable = $sm->get('Application\Model\CategoryTable');
        $this->activePaginator($categoryTable->fetchAll($orderby[0].' '.$orderby[1]));
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function productAction()
    {
        $this->layout()->pageTitle = "Product List";
        $this->_data['orederBy'] = $this->params()->fromRoute('orderby', 'ProductID-desc');
        $this->_data['categoryId'] = $this->params()->fromRoute('id', 1);
        $orderby = explode('-', $this->_data['orederBy']);
        $sm = $this->getServiceLocator();
        $categoryTable = $sm->get('Application\Model\CategoryTable');
        $productTable = $sm->get('Application\Model\ProductTable');
        
        $this->_data['categories'] = $categoryTable->fetchAll('CategoryID desc');
        $this->activePaginator($productTable->getByCategoryId($this->_data['categoryId'],$orderby[0].' '.$orderby[1]));
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function discountcodeAction()
    {
        $this->layout()->pageTitle = "Discount Code List";
        $this->_data['orederBy'] = $this->params()->fromRoute('orderby', 'DiscountCodeID-desc');
        $orderby = explode('-', $this->_data['orederBy']);
        $sm = $this->getServiceLocator();
        $cartTable = $sm->get('Application\Model\DiscountCodeTable');
        $this->activePaginator($cartTable->fetchAll($orderby[0].' '.$orderby[1]));
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function oldProductAction()
    {
        $this->layout()->pageTitle = "Old Product List";
        $this->_data['orederBy'] = $this->params()->fromRoute('orderby', 'ProductID-desc');
        $this->_data['categoryId'] = $this->params()->fromRoute('id', 1);
        $orderby = explode('-', $this->_data['orederBy']);
        $sm = $this->getServiceLocator();
        $categoryTable = $sm->get('Application\Model\CategoryTable');
        $productTable = $sm->get('Application\Model\ProductTable');
        
        $this->_data['categories'] = $categoryTable->fetchAll('CategoryID desc');
        $this->activePaginator($productTable->getByCategoryId($this->_data['categoryId'],$orderby[0].' '.$orderby[1]));
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function oldProductOptionAction()
    {
        $this->layout()->pageTitle = "Old Product Option List";
        $sm = $this->getServiceLocator();
        $categoryTable = $sm->get('Application\Model\CategoryTable');
        $optionTable = $sm->get('Application\Model\OptionTable');
        
        $this->_data['categories'] = $categoryTable->fetchAll('CategoryID desc');        
        $this->_data['productOptions'] = $optionTable->getAllOldOptions($sm);
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function editCategoryAction()
    {
        $categoryId = $this->params()->fromRoute('id', 1);
        $sm = $this->getServiceLocator();
        $categoryTable = $sm->get('Application\Model\CategoryTable');
        $category = $categoryTable->getById($categoryId);
        
        $categoryFrom = new \Admin\Form\CategoryForm($category);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $categoryFrom->setData($request->getPost());
            if ($categoryFrom->isValid()) {
                $categoryTable->save($categoryFrom->getData(), $categoryId);
                return $this->redirect()->toRoute('admin-store', array('action'=>'category'));
            }
        }
        
        return array('form'=>$categoryFrom);
    }
    
    public function newDiscountcodeAction()
    {
        $this->layout()->pageTitle = "New Discount Code";
        $sm = $this->getServiceLocator();
        $discountCodeTable = $sm->get('Application\Model\DiscountCodeTable');
        $discountCodeFrom = new \Admin\Form\DiscountCodeForm();
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $discountCodeFrom->setData($request->getPost());
            if ($discountCodeFrom->isValid()) {
                $discountCodeTable->save($discountCodeFrom->getData());
                return $this->redirect()->toRoute('admin-store', array('action'=>'discountcode'));
            }
        }
        $view = new ViewModel(array('form'=>$discountCodeFrom));
        $view->setTemplate('admin/store/discountcode-form');
        return $view;
    }
    
    public function editDiscountcodeAction()
    {
        $this->layout()->pageTitle = "Edit Discount Code";
        $codeId = $this->params()->fromRoute('id', 1);
        $sm = $this->getServiceLocator();
        $discountCodeTable = $sm->get('Application\Model\DiscountCodeTable');
        $discountCode = $discountCodeTable->getById($codeId);
        $discountCodeFrom = new \Admin\Form\DiscountCodeForm($discountCode);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $discountCodeFrom->setData($request->getPost());
            if ($discountCodeFrom->isValid()) {
                $discountCodeTable->save($discountCodeFrom->getData(), $codeId);
                return $this->redirect()->toRoute('admin-store', array('action'=>'discountcode'));
            }
        }
        $view = new ViewModel(array('form'=>$discountCodeFrom));
        $view->setTemplate('admin/store/discountcode-form');
        return $view;
    }
    
    public function oldProductEditAction()
    {
        $this->layout()->pageTitle = "Edit Product";
        $productId = $this->params()->fromRoute('id', 1);
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $product = $productTable->getById($productId);
        $productFrom = new \Admin\Form\OldProductForm($product);
        $request = $this->getRequest();
        if ($request->isPost()) { 
            $productFrom->setData($request->getPost());
            if ($productFrom->isValid()) {
                $productTable->save($productFrom->getData(), $productId);
                return $this->redirect()->toRoute('admin-store', array('action'=>'old-product'));
            }
        }
        $view = new ViewModel(array('form'=>$productFrom, 'product'=>$product));
        $view->setTemplate('admin/store/old-product-form');
        return $view;
    }
    
    public function deleteDiscountCodeAction(){
        $codeId = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $discountCodeTable = $sm->get('Application\Model\DiscountCodeTable');
        $discountCodeTable->deleteItem($codeId);
        return $this->redirect()->toRoute('admin-store', array('action'=>'discountcode'));
    }
    
    
    public function newProductAction(){
        $this->layout()->pageTitle = "New Product";
        $categoryId = $this->params()->fromRoute('id', 1);
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $productFrom = new \Admin\Form\ProductForm(array('CategoryID'=>$categoryId));
        $request = $this->getRequest();
        if ($request->isPost()) { 
            $productFrom->setData($request->getPost());
            if ($productFrom->isValid()) {
                $productTable->save($productFrom->getData());
                return $this->redirect()->toRoute('admin-store', array('action'=>'product'));
            }
        }
        $view = new ViewModel(array('form'=>$productFrom, 'product'=>array()));
        $view->setTemplate('admin/store/product-form');
        return $view;
    }
    
    public function editProductAction()
    {
        $this->layout()->pageTitle = "Edit Product";
        $productId = $this->params()->fromRoute('id', 1);
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $product = $productTable->getById($productId);
        $productFrom = new \Admin\Form\ProductForm($product);
        $request = $this->getRequest();
        if ($request->isPost()) { 
            $productFrom->setData($request->getPost());
            if ($productFrom->isValid()) {
                $productTable->save($productFrom->getData(), $productId);
                if($request->getPost('proceedTo')=='optionList')
                    return $this->redirect()->toRoute('admin-store', array('action'=>'option-group', 'id'=>$productId));
                else
                    return $this->redirect()->toRoute('admin-store', array('action'=>'product'));
            }
        }
        $view = new ViewModel(array('form'=>$productFrom, 'product'=>$product));
        $view->setTemplate('admin/store/product-form');
        return $view;
    }
    
    public function productPhotoAction(){
        $productId = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $product = $productTable->getById($productId);
        $productImageFrom = new \Admin\Form\ProductImageForm($productId);       
        $request = $this->getRequest();
        if ($request->isPost()) { 
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $productImageFrom->setData($post);
            if ($productImageFrom->isValid()) {
                $data = $productImageFrom->getData();
                $productTable->save(array('Photo1'=>'Y'), $productId);
                return $this->redirect()->toRoute('admin-store', array('action'=>'product'));
            }
        }
        $view = new ViewModel(array('form'=>$productImageFrom, 'product'=>$product));
        return $view;
    }
    
    public function copyProductAction(){
        $productId = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $product = $productTable->getById($productId);
        $request = $this->getRequest();
        if ($request->isPost()) { 
            $data = $request->getPost();
            $productTable->copy($product, $data, $sm);
            return $this->redirect()->toRoute('admin-store', array('action'=>'product', 'id'=>$product['CategoryID']));
        }
        $view = new ViewModel(array('product'=>$product));
        return $view;
    }
    
    public function deleteProductAction(){
        $productId = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $productTable->deleteItem($productId);
        return $this->redirect()->toRoute('admin-store', array('action'=>'product'));
    }
    
    public function optionGroupAction(){
        $productId = $productId = $this->params()->fromRoute('id', 1);
        $sm = $this->getServiceLocator();
        $productTable = $sm->get('Application\Model\ProductTable');
        $this->_data['product'] = $productTable->getById($productId);
        $this->layout()->pageTitle = "Options of ".$this->_data['product']['ProductName'];
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $this->_data['optionGroups'] = $optionGroupTable->getByProductId($productId);
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function newOptiongroupAction(){
        $this->layout()->pageTitle = "New Option";
        $productId = $this->params()->fromRoute('id');
        $sm = $this->getServiceLocator();
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $optionGroupFrom = new \Admin\Form\OptionGroupForm(array('ProductID'=>$productId));
        $request = $this->getRequest();
        if ($request->isPost()) { 
            $optionGroupFrom->setData($request->getPost());
            if ($optionGroupFrom->isValid()) {
                $optionGroupTable->save($optionGroupFrom->getData());
                return $this->redirect()->toRoute('admin-store', array('action'=>'option-group', 'id'=>$productId));
            }
        }
        $view = new ViewModel(array('form'=>$optionGroupFrom, 'product'=>array()));
        $view->setTemplate('admin/store/option-group-form');
        return $view;
    }
    
    public function editOptiongroupAction(){
        $this->layout()->pageTitle = "Edit Option";
        $optionGroupId = $this->params()->fromRoute('id');
        $sm = $this->getServiceLocator();
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $optionGroup = $optionGroupTable->getById($optionGroupId);
        $optionGroupFrom = new \Admin\Form\OptionGroupForm($optionGroup);
        $request = $this->getRequest();
        if ($request->isPost()) { 
            $optionGroupFrom->setData($request->getPost());
            if ($optionGroupFrom->isValid()) {
                $optionGroupTable->save($optionGroupFrom->getData(), $optionGroupId);
                if($request->getPost('proceedTo')=='optionList')
                    return $this->redirect()->toRoute('admin-store', array('action'=>'option-value', 'id'=>$optionGroupId));
                else
                    return $this->redirect()->toRoute('admin-store', array('action'=>'option-group', 'id'=>$optionGroup['ProductID']));
            }
        }
        $view = new ViewModel(array('form'=>$optionGroupFrom, 'product'=>array()));
        $view->setTemplate('admin/store/option-group-form');
        return $view;
    }
    
    public function deleteOptiongroupAction(){
        $optionGroupId = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $optionGroup = $optionGroupTable->getById($optionGroupId);
        $optionGroupTable->deleteItem($optionGroupId);
        return $this->redirect()->toRoute('admin-store', array('action'=>'option-group', 'id'=>$optionGroup['ProductID']));
    }
    
    public function optionValueAction(){
        $optionGroupId = $this->params()->fromRoute('id', 1);
        $sm = $this->getServiceLocator();
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        $productTable = $sm->get('Application\Model\ProductTable');
        $optionGroup = $optionGroupTable->getById($optionGroupId);
        $product = $productTable->getById($optionGroup['ProductID']);
        $this->layout()->pageTitle = "Values of Option ".$optionGroup['OptionGroupName'].' of Product '.$product['ProductName'];
        
        $optionTable = $sm->get('Application\Model\OptionTable');
        $this->_data['group'] = $optionGroup;
        $this->_data['options'] = $optionTable->getByGroupId($optionGroupId);
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function newOptionAction(){
        $this->layout()->pageTitle = "New Option";
        $optionGroupId = $this->params()->fromRoute('id');
        $sm = $this->getServiceLocator();
        $optionTable = $sm->get('Application\Model\OptionTable');
        $optionForm = new \Admin\Form\OptionForm(array('GroupOptionID'=>$optionGroupId));
        $request = $this->getRequest();
        if ($request->isPost()) { 
            $optionForm->setData($request->getPost());
            if ($optionForm->isValid()) {
                $optionTable->save($optionForm->getData());
                return $this->redirect()->toRoute('admin-store', array('action'=>'option-value', 'id'=>$optionGroupId));
            }
        }
        $view = new ViewModel(array('form'=>$optionForm));
        $view->setTemplate('admin/store/option-form');
        return $view;
    }
    
    public function editOptionAction(){
        $this->layout()->pageTitle = "Edit Option";
        $optionId = $this->params()->fromRoute('id');
        $sm = $this->getServiceLocator();
        $optionTable = $sm->get('Application\Model\OptionTable');
        $option = $optionTable->getById($optionId);
        $optionForm = new \Admin\Form\OptionForm($option);
        $request = $this->getRequest();
        if ($request->isPost()) { 
            $optionForm->setData($request->getPost());
            if ($optionForm->isValid()) {
                $optionTable->save($optionForm->getData(), $optionId);
                return $this->redirect()->toRoute('admin-store', array('action'=>'option-value', 'id'=>$option['GroupOptionID']));
            }
        }
        $view = new ViewModel(array('form'=>$optionForm));
        $view->setTemplate('admin/store/option-form');
        return $view;
    }
    
    public function deleteOptionAction(){
        $optionId = $this->params()->fromRoute('id', 0);
        $sm = $this->getServiceLocator();
        $optionTable = $sm->get('Application\Model\OptionTable');
        $option = $optionTable->getById($optionId);
        $optionTable->deleteItem($optionId);
        return $this->redirect()->toRoute('admin-store', array('action'=>'option-value', 'id'=>$option['GroupOptionID']));
    }
    
    public function editOldProductOptionAction()
    {
        $result = new ViewModel();
        $result->setTerminal(true);
        $optionId = $this->params()->fromRoute('id', 0);
        
        $sm = $this->getServiceLocator();
        $categoryTable = $sm->get('Application\Model\CategoryTable');
        $optionTable = $sm->get('Application\Model\OptionTable');
        $optionGroupTable = $sm->get('Application\Model\OptionGroupTable');
        
        $data = $this->getRequest()->getPost()->toArray();
        $optionTable->updateOld($data, $optionId);
        $option = $optionTable->getOldById($optionId);
        
        $option['optionGroups'] = $optionGroupTable->getByCategory($option['CategoryID']);
        $option['optionsInGroup'] = $optionTable->getBySimilatGroup($option['GroupOptionID']);
        
        $categories = $categoryTable->fetchAll('CategoryID desc');        
        $result->setVariables(array('option' => $option, 'categories'=>$categories));
        $result->setTemplate('admin/store/old-option');
        return $result;
    }
    
}

