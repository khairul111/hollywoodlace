<?php

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\Http\Headers;
use Zend\View\Model\ViewModel;

class MaintenanceController extends BaseController
{

    public function exportDatabaseAction()
    {
        $this->layout()->pageTitle = "Database Backup";
        $view = new ViewModel($this->_data);
        return $view;
    }
    
    public function executeExportAction(){
        $sm = $this->getServiceLocator();
        $config = $sm->get('config');
        $sqlFileName = "backup_".date("Ymd_His").".sql";
        $fileName = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."data".DIRECTORY_SEPARATOR."backup".DIRECTORY_SEPARATOR."backup_".date("Ymd_His").".sql";
        $command = "mysqldump hollywoodlace -u ".$config['db']['username']." -p".$config['db']['password']."  > ".$fileName;
        exec($command);
        $response = $this->getEvent()->getResponse();
        $response->setHeaders(Headers::fromString("Content-Type: application/octet-stream\r\nContent-Length: 9\r\nContent-Disposition: attachment; filename=\"".$sqlFileName."\""));

        $response->setContent(file_get_contents($fileName));
        unlink($fileName);
        return $response;
    }
    
}
